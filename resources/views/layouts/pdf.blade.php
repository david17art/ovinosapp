<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Certificado de Aprobacion de membresia</title>
    <style>
/* latin-ext */
@font-face {
  font-family: 'Rye';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: local('Rye Regular'), local('Rye-Regular'), url(https://fonts.gstatic.com/s/rye/v7/r05XGLJT86YzH57tbOo.woff2) format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Rye';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: local('Rye Regular'), local('Rye-Regular'), url(https://fonts.gstatic.com/s/rye/v7/r05XGLJT86YzEZ7t.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}        
        .content{
            margin:10%;
        }
        .text-center{
            text-align: center;
        }
        html{
            padding:0;
            margin:0;
        }
        body{
            margin:0;
            padding:0;
            border:15px solid #000;
        }
        .certificate-title{
            font-family: 'Rye', cursive;
            font-size: 3.5rem;
        }
        .text-justify{
            text-align:justify;
        }
    </style>
</head>
<body>
    <div class="content">
        @yield('content')
    </div>
</body>
</html>