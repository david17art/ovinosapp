<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <style>
        body{
            background:#f3f3f3;
        }
        .content{
            margin:0 15% 15% 15%;
            background:#fff;
            padding:4%;
        }
        .b-yellow {
            margin:0 15% 0 15%;
            border-top: 4px solid #f7df07;
        }

        .b-blue {
            margin:0 15% 0 15%;            
            border-top: 5px solid #000e88;
        }

        .b-red {
            margin:0 15% 0 15%;            
            border-top: 4px solid #be0505;
        }        
        .text-center{
            text-align: center;
        }
    </style>
</head>
<body>
    <p class="text-center">
        <img src="{{asset('img/logo-avco.png')}}" width="200px" alt="">
    </p>
    <div class="b-yellow"></div>
    <div class="b-blue"></div>
    <div class="b-red"></div>    
    <div class="content">
        @yield('content')
    </div>
</body>
</html>