<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF AVCO</title>

</head>
<style>
    body{
        /* background:url({{asset('img/logo-avco.jpeg')}}) */
    }
    header{
        padding:5%;
        display: flex;
    }
    .text-center{
        text-align: center;
    }

    .membrete{
        padding:1%;
    }

    th, td{
        border:1px solid black;
    }

</style>
<body>
    <header>
        <div>
            <img src="{{public_path().'/img/logo-avco.png'}}" width="120px" alt="">
        </div>
        <div class="text-center membrete">
            <h1>Certificado de Registro</h1>
            Asociación Venezolana de Criadores de Ovinos <br>
            Ministario del Poder Popular para la Agricultura Productiva y Tierras
        </div>
    </header>
    @yield('content')
</body>
</html>