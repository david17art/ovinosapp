<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    {{-- Materia Icons --}}
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">

    <style>
        body{
            background:url('/img/background.jpg');
            min-height: 720px;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('img/logo-avco.png')}}" width="50px" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @guest
                            
                        @else                        
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/home') }}">Inicio</a>
                            </li>                           
                            @if(Auth::user()->role == 'partner' && Auth::user()->status == 'aprobe' && Auth::user()->payed && Auth::user()->animal_module)
                                                         
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/partners/animals') }}">Animales</a>
                                </li>                                                                                                
                            @endif
                            @if(Auth::user()->role == 'superuser' || Auth::user()->role == 'admin')                                 
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/superAdmin/users') }}">Administradores / Asociados</a>
                                </li>                            
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/superAdmin/races') }}">Razas</a>
                                </li>                            
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/partners/animals') }}">Animales</a>
                                </li>   
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/payments') }}">Pagos</a>
                                </li>                                                                                                                                                    
                            @endif
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link"  href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @if(Auth::user()->role == 'partner')
                                <li class="nav-item">
                                    <a href="{{url('/members/certificate/'.Auth::user()->id)}}" target="_blank" title="Ver mi certificado" class="nav-link">
                                        <i class="fa fa-file"></i>
                                    </a>
                                </li>
                            @endif
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div class="b-yellow"></div>
        <div class="b-blue"></div>
        <div class="b-red"></div>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <div class="modal" id="modalDevInfo" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Información del sistema</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-justify">
                    Este es un sistema desarrollado para la <strong>Asociación Venezolana de Criadores de Ovinos</strong> por <a target="_blank" href="https://instagram.com/yosoydavidarteaga">David Arteaga</a> bajo la dirección de <a href="">Arturo Loaiza</a>
                </p>
                <p class="text-center">Inicio de Desarrollo: 31-11-2019 | Fin de desarrollo: 2020</p>
                <p class="text-center">
                    <strong>Hecho en Venezuela</strong>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
        </div>    
</body>
</html>
