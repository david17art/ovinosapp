<h3>Datos Registrado</h3>



<form action="">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="" class="control-label">Nombre</label>
                <input type="text" disabled class="form-control" value="{{$user->name}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="" class="control-label">Cedula</label>
                <input type="text" disabled class="form-control" value="{{$user->identification}}">
            </div>             
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="" class="control-label">Nombre de empresa</label>
                <input type="text" disabled class="form-control" value="{{$user->company}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="" class="control-label">Numero de telefono</label>
                <input type="text" disabled class="form-control" value="{{$user->phone}}">
            </div>             
        </div> 
        <div class="col-md-6">
            <div class="form-group">
                <label for="" class="control-label">Nombre de predio</label>
                <input type="text" disabled class="form-control" value="{{$user->predio}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="" class="control-label">Email</label>
                <input type="text" disabled class="form-control" value="{{$user->email}}">
            </div>             
        </div>   
        <div class="col-md-4">
            <div class="form-group">
                <label for="" class="control-label">Nomenclatura 1</label>
                <input type="text" disabled class="form-control" value="{{$user->nomenclatureSuggest1}}">
            </div>             
        </div>                        
        <div class="col-md-4">
            <div class="form-group">
                <label for="" class="control-label">Nomenclatura 2</label>
                <input type="text" disabled class="form-control" value="{{$user->nomenclatureSuggest2}}">
            </div>             
        </div>                                
        <div class="col-md-4">
            <div class="form-group">
                <label for="" class="control-label">Nomenclatura 3</label>
                <input type="text" disabled class="form-control" value="{{$user->nomenclatureSuggest3}}">
            </div>             
        </div>                                
    </div>
</form>