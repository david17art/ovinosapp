@extends('layouts.emails')

@section('title')
    Notificacion
@endsection
@section('content')
    @if($mode == 0)
        Hola su solicitud de membresia está siendo procesada por nuestros administradores en los dias venideros tendrás noticias acerca de nosotros
    @elseif($mode == 1)
        Hola este correo es para notificarle que se le ha enviado una solicitud de membresia por favor revisar cuanto antes
    @elseif($mode == 2)
        Hola hemos registrado un pago en el sistema, por favor verificar en cuanto le sea posible
    @elseif($mode == 3)
        Hola Tenemos malas noticias acerca de su pago, lo hemos tenido que rechazar dado a que no cumple los requisitos
    @elseif($mode == 4)
        Hola Tenemos Buenas noticias acerca de su pago, Lo hemos aprobado correctamente
    @elseif($mode == 5)
        Hola se ha registrado un animal en el sistema
    @elseif($mode == 6)
        Hola, tenemos buenas noticias, hemos aprobado a su animal
    @elseif($mode == 7)
        Hola, tenemos malas noticias, hemos rechazado a su animal
    @elseif($mode == 8)
        Hola, hemos aprobado su pago de membresia correctamente
    @elseif($mode == 9)
        Hola, hemos aprobado su pago, ya puede proceder a registrar sus animales. 
    @endif
@endsection