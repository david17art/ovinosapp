@extends('layouts.emails')

@section('title')
    Correo de Aprobacion de Membresia
@endsection
@section('content')
    Hola, {{$client->name}} hemos aprobado su solicitud de membresia, puede acceder al sistema desde <a href="{{url('/login')}}">Este enlace</a> con los datos proporcionados en el registro. <br>
    Tambien puedes ver el certificado de membresia desde <a href="{{url('/members/certificate/'.$client->id)}}">este enlace</a>
@endsection