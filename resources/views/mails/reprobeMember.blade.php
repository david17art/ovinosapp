@extends('layouts.emails')

@section('title')
    Correo de rechazo de Membresia
@endsection
@section('content')
    Hola, {{$client->name}} lo sentimos, rechazamos su solicitud de membresia, para volver a solicitarla, hacerlo desde <a href="{{url('/register')}}">aquí</a>
@endsection