@extends('layouts.pdf-avco')

@section('content')

        <div>
            <table>
                <tr>PO Puros de origen</tr>
            </table>
            <table align="right">
                <tr>
                    <td>ID Nombre del animal</td>
                    <td>{{$animal->id}} {{$animal->name}}</td>
                </tr>
                <tr>
                    <td>Tipo Racial</td>
                    <td>{{$animal->raza->name}}</td>
                </tr> 
                <tr>
                    <td>Fecha de nacimiento</td>
                    <td>{{$animal->birthdate}}</td>
                </tr> 
                <tr>
                    <td>Forma de concepcion</td>
                    <td>{{$animal->parto}}</td>
                </tr>  
                <tr>
                    <td>Propietario</td>
                    <td>{{$animal->property}}</td>
                </tr>              
                <tr>
                    <td>Predio</td>
                    <td>{{$animal->partner->predio}}</td>
                </tr>   
                <tr>
                    <td>Sexo</td>
                    <td>{{$animal->gender}}</td>
                </tr>      
                <tr>
                    <td>Categoria</td>
                    <td>{{$animal->generation}}</td>
                </tr>  
                <tr>
                    <td>Tecnico Clasificador</td>
                    <td>{{$animal->clasificador}}</td>
                </tr>  
                <tr>
                    <td>Fecha de otorgamiento</td>
                    <td>{{$animal->otorgamiento}}</td>
                </tr>                                                                                             
            </table>  
        </div>

        <table>
            <tr>
                <td>Padre</td>
                <td>{{$animal->dad->name}}</td>
            </tr>
            <tr>
                <td>Numero de registro</td>
                <td>{{$animal->dad->id}}</td>
            </tr>                        
        </table>
        @if($animal['dad']['dad'] && $animal['dad']['mom'])
        <table align="center">
            <tr>
                <td>Abuelo</td>
                <td>{{$animal->dad->dad->name}}</td>
            </tr>
            <tr>
                <td>Numero de registro</td>
                <td>{{$animal->dad->dad->id}}</td>
            </tr>  
            <tr>
                <td>Abuela</td>
                <td>{{$animal->dad->mom->name}}</td>
            </tr>
            <tr>
                <td>Numero de registro</td>
                <td>{{$animal->dad->mom->id}}</td>
            </tr>                                   
        </table> 
      @endif
        <table>
            <tr>
                <td>Madre</td>
                <td>{{$animal->mom->name}}</td>
            </tr>
            <tr>
                <td>Numero de registro</td>
                <td>{{$animal->mom->id}}</td>
            </tr>                        
        </table>   

        @if($animal['mom']['dad'] && $animal['mom']['mom'])
            <table align="center">
                <tr>
                    <td>Abuelo</td>
                    <td>{{$animal->mom->dad->name}}</td>
                </tr>
                <tr>
                    <td>Numero de registro</td>
                    <td>{{$animal->mom->dad->id}}</td>
                </tr>  
                <tr>
                    <td>Abuela</td>
                    <td>{{$animal->mom->mom->name}}</td>
                </tr>
                <tr>
                    <td>Numero de registro</td>
                    <td>{{$animal->mom->mom->id}}</td>
                </tr>                                   
            </table>  
        @endif
@endsection