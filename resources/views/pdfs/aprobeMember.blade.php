@extends('layouts.pdf')

@section('content')
    <img src="{{ public_path().'/img/certificado_text.jpg' }}" width="100%" alt="">
    <p class="text-center"><strong>{{$client->nomenclature}}</strong></p>
    <p class="text-center">
        <strong>ASOCIACIÓN VENEZOLANA DE CRIADORES DE OVEJA</strong>
    </p>
    <h3 class="text-center">{{$client->company}}</h3>
    <p class="text-center">CI: {{$client->identification}}</p>
    <p class="text-justify">
        El certificado {{$client->nomenclature}} se ha registrado en la ASOCIACIÓN VENEZOLANA DE CRIADORES DE OVEJA como el nombre de tu granja  {{$client->company}} Debe usar su tatuaje de rebaño registrado {{$client->nomenclature}} en la oreja derecha, la cola derecha o la cola central de todos los animales nacidos en su rebaño.  Los miembros deben utilizar el número correlativo según el nacimiento en el año y agregar la letra que la asociacion decipne por año la secuencia asignada.  Puede autorizar a otros miembros a usar su tatuaje de rebaño registrado presentando una tarjeta de autorización de tatuaje con AVCO. Ningún animal será registrado por AVCO con el mismo tatuaje.  Si su membresía AVCO caduca, pero se restablece dentro de los tres años posteriores a su vencimiento, puede continuar usando esta secuencia como su tatuaje de rebaño.  Si la secuencia de tatuajes anterior es inaceptable, puede solicitar un cambio en cualquier momento dentro de los 30 días posteriores a la certificación Certificado: {{date('Y-m-d')}} Venezuela Teléfono ##########, martes __ de mayo de 2020 DIRECCIÓN DE LA ASOCIACION Teléfono 0000000 Fax ooooooo correo creada en el año {{date('Y')}}
    </p>
    <p class="text-center"><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->generate(url('/members/certificate/'.$client->id))) !!} "></p>

@endsection