@extends('layouts.app')

@section('title')
    Animales
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Animales</div>

                <div class="card-body">
                    <animals-index user="{{Auth::user()}}" />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
