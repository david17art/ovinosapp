@extends('layouts.app')

@section('title')
    Registro
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombres') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{__('Cedula de identidad')}}</label>
                            <div class="col-md-6">
                                <input id="identification" type="number" class="form-control{{ $errors->has('identification') ? ' is-invalid' : '' }}" name="identification" value="{{ old('identification') }}" required autofocus>

                                @if ($errors->has('identification'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('identification') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{__('Numero telefonico')}}</label>
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div>                        
                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{__('Nombre de la empresa')}}</label>
                            <div class="col-md-6">
                                <input id="company" type="text" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" name="company" value="{{ old('company') }}" required autofocus>

                                @if ($errors->has('company'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div> 
                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{__('Nombre del predio')}}</label>
                            <div class="col-md-6">
                                <input id="predio" type="text" class="form-control{{ $errors->has('predio') ? ' is-invalid' : '' }}" name="predio" value="{{ old('predio') }}" required autofocus>

                                @if ($errors->has('predio'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('predio') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div> 
                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{__('Nomenclatura 1')}}</label>
                            <div class="col-md-6">
                                <input id="nomenclatureSuggest1" maxlength="4" type="text" class="form-control{{ $errors->has('nomenclatureSuggest1') ? ' is-invalid' : '' }}" name="nomenclatureSuggest1" value="{{ old('nomenclatureSuggest1') }}" required autofocus>

                                @if ($errors->has('nomenclatureSuggest1'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nomenclatureSuggest1') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div> 
                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{__('Nomenclatura 2')}}</label>
                            <div class="col-md-6">
                                <input id="nomenclatureSuggest2" maxlength="4" type="text" class="form-control{{ $errors->has('nomenclatureSuggest2') ? ' is-invalid' : '' }}" name="nomenclatureSuggest2" value="{{ old('nomenclatureSuggest2') }}" required autofocus>

                                @if ($errors->has('nomenclatureSuggest2'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nomenclatureSuggest2') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div> 
                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{__('Nomenclatura 3')}}</label>
                            <div class="col-md-6">
                                <input id="nomenclatureSuggest3" type="text" maxlength="4" class="form-control{{ $errors->has('nomenclatureSuggest3') ? ' is-invalid' : '' }}" name="nomenclatureSuggest3" value="{{ old('nomenclatureSuggest3') }}" required autofocus>

                                @if ($errors->has('nomenclatureSuggest3'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nomenclatureSuggest3') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div>                                                                                                 
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $email ?? '' }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $password ?? '' }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group">
                            <h3 style="padding:10px;text-align:center;background:#f3f3f3;font-weight:bold;">
                                Registro de pago
                            </h3>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="control-label">Referencia</label>
                                    <input type="text" name="refer" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="control-label">Monto</label>
                                    <input type="number" name="amount" required step="0.000001" class="form-control">
                                </div>
                            </div>
                            <?php 

                                use App\Bank;

                                $banks = Bank::all();

                            ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="control-label">Bancos</label>
                                    <select name="bank" id="" class="form-control" required>
                                        <option value="">Seleccionar</option>
                                        @foreach($banks as $bank)
                                            <option value="{{$bank->name}}">{{$bank->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
