@extends('layouts.app')

@section('title')   
    Pagos
@endsection

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Pagos</div>
    
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <payment-index></payment-index>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection