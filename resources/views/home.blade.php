@extends('layouts.app')

@section('title')
    Inicio
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Inicio</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if($user->role == 'partner')
                        {{-- @include('layouts.sections.homePartnerNotAprobe') --}}
                        @if($user->status == 'reprobe' )
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        Pago de Membresia
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>Estatus</th>
                                                    <th>Referencia</th>
                                                    <th>Monto</th>
                                                    <th>Banco</th>
                                                </tr>
                                                @if(isset($payment))
                                                    <tr>
                                                        <td>
                                                            @if($payment->status == 'aprobe')
                                                                <span style="color:green;"><i class="fa fa-circle"></i></span> Aprobado 
                                                            @elseif($payment->status == 'reprobe')
                                                                <span style="color:red;"><i class="fa fa-circle"></i></span> Rechazado 
                                                            @else
                                                                <span style="color:yellow;"><i class="fa fa-circle"></i></span> En revision 
                                                            @endif
                                                        </td>                                                        
                                                        <td>{{$payment->refer}}</td>
                                                        <td>{{$payment->amount}}</td>
                                                        <td>{{$payment->bank}}</td>

                                                    </tr>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        <span style="color:red;" class="text-center">
                            <p class="text-center">
                            <strong>Nota: </strong>Aun no te han aprobado la membresia, nos comunicaremos contigo en cuanto este proceso haya finalizado...
                            </p>
                        </span>
                        @else
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-header">
                                            Datos de registros
                                        </div>
                                        <div class="card-body">
                                            <complete-info-users></complete-info-users>
                                        </div>
                                        <div class="card-footer">
                                            @if(Auth::user()->completed)
                                                <span style="color:#1abc9c;font-weight:bold;">REGISTRO COMPLETO</span>
                                            @else
                                                <span style="color:red;"><strong>NOTA: </strong>debes ingresar los datos para poder completar tu registro</span>
                                            @endif
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-header">
                                            Pago de Membresia
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Estatus</th>
                                                        <th>Referencia</th>
                                                        <th>Monto</th>
                                                        <th>Banco</th>
                                                    </tr>
                                                @if(isset($payment))
                                                    <tr>
                                                        <td>
                                                            @if($payment->status == 'aprobe')
                                                                <span style="color:green;"><i class="fa fa-circle"></i></span> Aprobado 
                                                            @elseif($payment->status == 'reprobe')
                                                                <span style="color:red;"><i class="fa fa-circle"></i></span> Rechazado 
                                                            @else
                                                                <span style="color:yellow;"><i class="fa fa-circle"></i></span> En revision 
                                                            @endif
                                                        </td>                                                        
                                                        <td>{{$payment->refer}}</td>
                                                        <td>{{$payment->amount}}</td>
                                                        <td>{{$payment->bank}}</td>

                                                    </tr>
                                                @endif
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="card">
                                        <div class="card-header">
                                            Pago para el registro de Animales
                                        </div>
                                        <div class="card-body">
                                            <payments mode="animal_request" id="{{Auth::user()->id}}" ></payments>
                                        </div>
                                        <div class="card-footer">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif   
                        
                    @elseif($user->role == 'superuser' || $user->role == 'admin')
                        <dashboard></dashboard>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
