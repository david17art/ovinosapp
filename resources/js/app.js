/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Buefy from 'buefy'
import '@mdi/font/css/materialdesignicons.css'
import 'buefy/dist/buefy.css'
import VueSweetalert2 from 'vue-sweetalert2';
import Race from './components/superusers/Race'
import AnimalIndex from './components/partners/animals/Index'
import UserIndex from './components/superusers/users/Index';
import * as VeeValidate from 'vee-validate';
import CompleteInfoUser from './components/partners/users/CompleteInfo'
import Payments from './components/Payments'
import Dashboard from './components/superusers/Dashboard'
import PaymentIndex from './components/superusers/payments/Index'

Vue.use(VeeValidate);
Vue.use(VueSweetalert2);
Vue.use(Buefy)
    /**
     * The following block of code may be used to automatically register your
     * Vue components. It will recursively scan this directory for the Vue
     * components and automatically register them with their "basename".
     *
     * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
     */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('aprobe-member', require('./components/home/AprobeMember.vue').default);
Vue.component('races', Race);
Vue.component('animals-index', AnimalIndex);
Vue.component('users-index', UserIndex);
Vue.component('complete-info-users', CompleteInfoUser);
Vue.component('payments', Payments);
Vue.component('Dashboard', Dashboard);
Vue.component('PaymentIndex', PaymentIndex);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});