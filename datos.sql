-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.36-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para ovinosdb
CREATE DATABASE IF NOT EXISTS `ovinosdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ovinosdb`;

-- Volcando estructura para tabla ovinosdb.animals
CREATE TABLE IF NOT EXISTS `animals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('macho','hembra') COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `weight` double(8,2) NOT NULL,
  `parto` enum('simple','double','triple','multiple') COLLATE utf8mb4_unicode_ci NOT NULL,
  `dad_id` int(11) DEFAULT NULL,
  `mom_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `race_id` int(11) NOT NULL,
  `tatto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('revision','aprobado','rechazado') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'revision',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `criador` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generation` int(11) DEFAULT NULL,
  `otorgamiento` date DEFAULT NULL,
  `clasificador` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `animals_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.animals: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `animals` DISABLE KEYS */;
INSERT INTO `animals` (`id`, `name`, `gender`, `birthdate`, `weight`, `parto`, `dad_id`, `mom_id`, `user_id`, `race_id`, `tatto`, `status`, `created_at`, `updated_at`, `completed`, `criador`, `property`, `generation`, `otorgamiento`, `clasificador`, `payed`) VALUES
	(1, 'Padre 1', 'macho', '2017-08-08', 10.00, 'simple', NULL, NULL, NULL, 1, 'P1', 'aprobado', '2019-12-21 18:46:44', '2019-12-21 18:46:44', 0, NULL, NULL, NULL, NULL, NULL, 0),
	(2, 'Padre 2', 'macho', '2017-08-08', 10.00, 'simple', NULL, NULL, NULL, 2, 'P1', 'aprobado', '2019-12-21 18:46:44', '2019-12-21 18:46:44', 0, NULL, NULL, NULL, NULL, NULL, 0),
	(3, 'Madre 1', 'hembra', '2017-08-08', 10.00, 'simple', NULL, NULL, NULL, 2, 'M1', 'aprobado', '2019-12-21 18:46:44', '2019-12-21 18:46:44', 0, NULL, NULL, NULL, NULL, NULL, 0),
	(4, 'Madre 2', 'hembra', '2017-08-08', 10.00, 'simple', NULL, NULL, NULL, 2, 'M2', 'aprobado', '2019-12-21 18:46:44', '2019-12-21 18:46:44', 0, NULL, NULL, NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `animals` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.countries: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Venezuela', '2019-12-21 18:46:44', '2019-12-21 18:46:44');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.migrations: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_05_062622_create_nomenclatures_table', 1),
	(5, '2019_12_11_061443_create_races_table', 1),
	(6, '2019_12_13_034943_create_animals_table', 1),
	(7, '2019_12_20_035942_add_completed_member_on_table_users', 1),
	(8, '2019_12_20_040254_add_completed_animal_on_table_animals', 1),
	(9, '2019_12_20_041437_create_countries_table', 1),
	(10, '2019_12_20_041449_create_states_table', 1),
	(11, '2019_12_20_041501_create_towns_table', 1),
	(12, '2019_12_21_042634_create_payments_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.nomenclatures
CREATE TABLE IF NOT EXISTS `nomenclatures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `selected` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nomenclatures_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.nomenclatures: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `nomenclatures` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomenclatures` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.payments
CREATE TABLE IF NOT EXISTS `payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `refer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(12,2) NOT NULL,
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `animal_id` int(11) DEFAULT NULL,
  `status` enum('aprobe','reprobe','revision') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'revision',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.payments: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.races
CREATE TABLE IF NOT EXISTS `races` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.races: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `races` DISABLE KEYS */;
INSERT INTO `races` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Raza 1', '2019-12-21 18:46:44', '2019-12-21 18:46:44'),
	(2, 'Raza 2', '2019-12-21 18:46:44', '2019-12-21 18:46:44'),
	(3, 'Raza 3', '2019-12-21 18:46:44', '2019-12-21 18:46:44');
/*!40000 ALTER TABLE `races` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.states
CREATE TABLE IF NOT EXISTS `states` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.states: ~25 rows (aproximadamente)
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Amazonas', '2019-12-21 18:46:44', '2019-12-21 18:46:44'),
	(2, 1, 'Anzoátegui', '2019-12-21 18:46:44', '2019-12-21 18:46:44'),
	(3, 1, 'Apure', '2019-12-21 18:46:44', '2019-12-21 18:46:44'),
	(4, 1, 'Aragua', '2019-12-21 18:46:44', '2019-12-21 18:46:44'),
	(5, 1, 'Barinas', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(6, 1, 'Bolívar', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(7, 1, 'Carabobo', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(8, 1, 'Cojedes', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(9, 1, 'Delta Amacuro', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(10, 1, 'Falcón', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(11, 1, 'Guárico', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(12, 1, 'Lara', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(13, 1, 'Mérida', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(14, 1, 'Miranda', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(15, 1, 'Monagas', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(16, 1, 'Nueva Esparta', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(17, 1, 'Portuguesa', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(18, 1, 'Sucre', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(19, 1, 'Táchira', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(20, 1, 'Trujillo', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(21, 1, 'Vargas', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(22, 1, 'Yaracuy', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(23, 1, 'Zulia', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(24, 1, 'Distrito Capital', '2019-12-21 18:46:45', '2019-12-21 18:46:45'),
	(25, 1, 'Dependencias Federales', '2019-12-21 18:46:45', '2019-12-21 18:46:45');
/*!40000 ALTER TABLE `states` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.towns
CREATE TABLE IF NOT EXISTS `towns` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.towns: ~306 rows (aproximadamente)
/*!40000 ALTER TABLE `towns` DISABLE KEYS */;
INSERT INTO `towns` (`id`, `name`, `state_id`, `created_at`, `updated_at`) VALUES
	(1, 'Alto Orinoco', 1, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(2, 'Atabapo', 1, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(3, 'Atures', 1, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(4, 'Autana', 1, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(5, 'Manapiare', 1, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(6, 'Maroa', 1, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(7, 'Río Negro', 1, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(8, 'Anaco', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(9, 'Aragua', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(10, 'Manuel Ezequiel Bruzual', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(11, 'Diego Bautista Urbaneja', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(12, 'Fernando Peñalver', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(13, 'Francisco Del Carmen Carvajal', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(14, 'General Sir Arthur McGregor', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(15, 'Guanta', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(16, 'Independencia', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(17, 'José Gregorio Monagas', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(18, 'Juan Antonio Sotillo', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(19, 'Juan Manuel Cajigal', 2, '2019-12-21 18:46:46', '2019-12-21 18:46:46'),
	(20, 'Libertad', 2, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(21, 'Francisco de Miranda', 2, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(22, 'Pedro María Freites', 2, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(23, 'Píritu', 2, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(24, 'San José de Guanipa', 2, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(25, 'San Juan de Capistrano', 2, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(26, 'Santa Ana', 2, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(27, 'Simón Bolívar', 2, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(28, 'Simón Rodríguez', 2, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(29, 'Achaguas', 3, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(30, 'Biruaca', 3, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(31, 'Muñóz', 3, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(32, 'Páez', 3, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(33, 'Pedro Camejo', 3, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(34, 'Rómulo Gallegos', 3, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(35, 'San Fernando', 3, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(36, 'Atanasio Girardot', 4, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(37, 'Bolívar', 4, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(38, 'Camatagua', 4, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(39, 'Francisco Linares Alcántara', 4, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(40, 'José Ángel Lamas', 4, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(41, 'José Félix Ribas', 4, '2019-12-21 18:46:47', '2019-12-21 18:46:47'),
	(42, 'José Rafael Revenga', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(43, 'Libertador', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(44, 'Mario Briceño Iragorry', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(45, 'Ocumare de la Costa de Oro', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(46, 'San Casimiro', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(47, 'San Sebastián', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(48, 'Santiago Mariño', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(49, 'Santos Michelena', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(50, 'Sucre', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(51, 'Tovar', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(52, 'Urdaneta', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(53, 'Zamora', 4, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(54, 'Alberto Arvelo Torrealba', 5, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(55, 'Andrés Eloy Blanco', 5, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(56, 'Antonio José de Sucre', 5, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(57, 'Arismendi', 5, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(58, 'Barinas', 5, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(59, 'Bolívar', 5, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(60, 'Cruz Paredes', 5, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(61, 'Ezequiel Zamora', 5, '2019-12-21 18:46:48', '2019-12-21 18:46:48'),
	(62, 'Obispos', 5, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(63, 'Pedraza', 5, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(64, 'Rojas', 5, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(65, 'Sosa', 5, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(66, 'Caroní', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(67, 'Cedeño', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(68, 'El Callao', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(69, 'Gran Sabana', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(70, 'Heres', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(71, 'Piar', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(72, 'Angostura (Raúl Leoni)', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(73, 'Roscio', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(74, 'Sifontes', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(75, 'Sucre', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(76, 'Padre Pedro Chien', 6, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(77, 'Bejuma', 7, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(78, 'Carlos Arvelo', 7, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(79, 'Diego Ibarra', 7, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(80, 'Guacara', 7, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(81, 'Juan José Mora', 7, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(82, 'Libertador', 7, '2019-12-21 18:46:49', '2019-12-21 18:46:49'),
	(83, 'Los Guayos', 7, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(84, 'Miranda', 7, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(85, 'Montalbán', 7, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(86, 'Naguanagua', 7, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(87, 'Puerto Cabello', 7, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(88, 'San Diego', 7, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(89, 'San Joaquín', 7, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(90, 'Valencia', 7, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(91, 'Anzoátegui', 8, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(92, 'Tinaquillo', 8, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(93, 'Girardot', 8, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(94, 'Lima Blanco', 8, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(95, 'Pao de San Juan Bautista', 8, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(96, 'Ricaurte', 8, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(97, 'Rómulo Gallegos', 8, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(98, 'San Carlos', 8, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(99, 'Tinaco', 8, '2019-12-21 18:46:50', '2019-12-21 18:46:50'),
	(100, 'Antonio Díaz', 9, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(101, 'Casacoima', 9, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(102, 'Pedernales', 9, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(103, 'Tucupita', 9, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(104, 'Acosta', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(105, 'Bolívar', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(106, 'Buchivacoa', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(107, 'Cacique Manaure', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(108, 'Carirubana', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(109, 'Colina', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(110, 'Dabajuro', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(111, 'Democracia', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(112, 'Falcón', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(113, 'Federación', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(114, 'Jacura', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(115, 'José Laurencio Silva', 10, '2019-12-21 18:46:51', '2019-12-21 18:46:51'),
	(116, 'Los Taques', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(117, 'Mauroa', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(118, 'Miranda', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(119, 'Monseñor Iturriza', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(120, 'Palmasola', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(121, 'Petit', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(122, 'Píritu', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(123, 'San Francisco', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(124, 'Sucre', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(125, 'Tocópero', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(126, 'Unión', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(127, 'Urumaco', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(128, 'Zamora', 10, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(129, 'Camaguán', 11, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(130, 'Chaguaramas', 11, '2019-12-21 18:46:52', '2019-12-21 18:46:52'),
	(131, 'El Socorro', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(132, 'José Félix Ribas', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(133, 'José Tadeo Monagas', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(134, 'Juan Germán Roscio', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(135, 'Julián Mellado', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(136, 'Las Mercedes', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(137, 'Leonardo Infante', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(138, 'Pedro Zaraza', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(139, 'Ortíz', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(140, 'San Gerónimo de Guayabal', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(141, 'San José de Guaribe', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(142, 'Santa María de Ipire', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(143, 'Sebastián Francisco de Miranda', 11, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(144, 'Andrés Eloy Blanco', 12, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(145, 'Crespo', 12, '2019-12-21 18:46:53', '2019-12-21 18:46:53'),
	(146, 'Iribarren', 12, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(147, 'Jiménez', 12, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(148, 'Morán', 12, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(149, 'Palavecino', 12, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(150, 'Simón Planas', 12, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(151, 'Torres', 12, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(152, 'Urdaneta', 12, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(153, 'Alberto Adriani', 13, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(154, 'Andrés Bello', 13, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(155, 'Antonio Pinto Salinas', 13, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(156, 'Aricagua', 13, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(157, 'Arzobispo Chacón', 13, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(158, 'Campo Elías', 13, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(159, 'Caracciolo Parra Olmedo', 13, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(160, 'Cardenal Quintero', 13, '2019-12-21 18:46:54', '2019-12-21 18:46:54'),
	(161, 'Guaraque', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(162, 'Julio César Salas', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(163, 'Justo Briceño', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(164, 'Libertador', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(165, 'Miranda', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(166, 'Obispo Ramos de Lora', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(167, 'Padre Noguera', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(168, 'Pueblo Llano', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(169, 'Rangel', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(170, 'Rivas Dávila', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(171, 'Santos Marquina', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(172, 'Sucre', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(173, 'Tovar', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(174, 'Tulio Febres Cordero', 13, '2019-12-21 18:46:55', '2019-12-21 18:46:55'),
	(175, 'Zea', 13, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(176, 'Acevedo', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(177, 'Andrés Bello', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(178, 'Baruta', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(179, 'Brión', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(180, 'Buroz', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(181, 'Carrizal', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(182, 'Chacao', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(183, 'Cristóbal Rojas', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(184, 'El Hatillo', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(185, 'Guaicaipuro', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(186, 'Independencia', 14, '2019-12-21 18:46:56', '2019-12-21 18:46:56'),
	(187, 'Lander', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(188, 'Los Salias', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(189, 'Páez', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(190, 'Paz Castillo', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(191, 'Pedro Gual', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(192, 'Plaza', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(193, 'Simón Bolívar', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(194, 'Sucre', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(195, 'Urdaneta', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(196, 'Zamora', 14, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(197, 'Acosta', 15, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(198, 'Aguasay', 15, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(199, 'Bolívar', 15, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(200, 'Caripe', 15, '2019-12-21 18:46:57', '2019-12-21 18:46:57'),
	(201, 'Cedeño', 15, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(202, 'Ezequiel Zamora', 15, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(203, 'Libertador', 15, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(204, 'Maturín', 15, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(205, 'Piar', 15, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(206, 'Punceres', 15, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(207, 'Santa Bárbara', 15, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(208, 'Sotillo', 15, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(209, 'Uracoa', 15, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(210, 'Antolín del Campo', 16, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(211, 'Arismendi', 16, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(212, 'García', 16, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(213, 'Gómez', 16, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(214, 'Maneiro', 16, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(215, 'Marcano', 16, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(216, 'Mariño', 16, '2019-12-21 18:46:58', '2019-12-21 18:46:58'),
	(217, 'Península de Macanao', 16, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(218, 'Tubores', 16, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(219, 'Villalba', 16, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(220, 'Díaz', 16, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(221, 'Agua Blanca', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(222, 'Araure', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(223, 'Esteller', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(224, 'Guanare', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(225, 'Guanarito', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(226, 'Monseñor José Vicente de Unda', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(227, 'Ospino', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(228, 'Páez', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(229, 'Papelón', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(230, 'San Genaro de Boconoíto', 17, '2019-12-21 18:46:59', '2019-12-21 18:46:59'),
	(231, 'San Rafael de Onoto', 17, '2019-12-21 18:47:00', '2019-12-21 18:47:00'),
	(232, 'Santa Rosalía', 17, '2019-12-21 18:47:00', '2019-12-21 18:47:00'),
	(233, 'Sucre', 17, '2019-12-21 18:47:00', '2019-12-21 18:47:00'),
	(234, 'Turén', 17, '2019-12-21 18:47:00', '2019-12-21 18:47:00'),
	(235, 'Andrés Eloy Blanco', 18, '2019-12-21 18:47:00', '2019-12-21 18:47:00'),
	(236, 'Andrés Mata', 18, '2019-12-21 18:47:00', '2019-12-21 18:47:00'),
	(237, 'Arismendi', 18, '2019-12-21 18:47:00', '2019-12-21 18:47:00'),
	(238, 'Benítez', 18, '2019-12-21 18:47:00', '2019-12-21 18:47:00'),
	(239, 'Bermúdez', 18, '2019-12-21 18:47:01', '2019-12-21 18:47:01'),
	(240, 'Bolívar', 18, '2019-12-21 18:47:01', '2019-12-21 18:47:01'),
	(241, 'Cajigal', 18, '2019-12-21 18:47:01', '2019-12-21 18:47:01'),
	(242, 'Cruz Salmerón Acosta', 18, '2019-12-21 18:47:01', '2019-12-21 18:47:01'),
	(243, 'Libertador', 18, '2019-12-21 18:47:01', '2019-12-21 18:47:01'),
	(244, 'Mariño', 18, '2019-12-21 18:47:01', '2019-12-21 18:47:01'),
	(245, 'Mejía', 18, '2019-12-21 18:47:01', '2019-12-21 18:47:01'),
	(246, 'Montes', 18, '2019-12-21 18:47:01', '2019-12-21 18:47:01'),
	(247, 'Ribero', 18, '2019-12-21 18:47:02', '2019-12-21 18:47:02'),
	(248, 'Sucre', 18, '2019-12-21 18:47:02', '2019-12-21 18:47:02'),
	(249, 'Valdéz', 18, '2019-12-21 18:47:02', '2019-12-21 18:47:02'),
	(250, 'Andrés Bello', 19, '2019-12-21 18:47:02', '2019-12-21 18:47:02'),
	(251, 'Antonio Rómulo Costa', 19, '2019-12-21 18:47:02', '2019-12-21 18:47:02'),
	(252, 'Ayacucho', 19, '2019-12-21 18:47:02', '2019-12-21 18:47:02'),
	(253, 'Bolívar', 19, '2019-12-21 18:47:02', '2019-12-21 18:47:02'),
	(254, 'Cárdenas', 19, '2019-12-21 18:47:02', '2019-12-21 18:47:02'),
	(255, 'Córdoba', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(256, 'Fernández Feo', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(257, 'Francisco de Miranda', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(258, 'García de Hevia', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(259, 'Guásimos', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(260, 'Independencia', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(261, 'Jáuregui', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(262, 'José María Vargas', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(263, 'Junín', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(264, 'Libertad', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(265, 'Libertador', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(266, 'Lobatera', 19, '2019-12-21 18:47:03', '2019-12-21 18:47:03'),
	(267, 'Michelena', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(268, 'Panamericano', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(269, 'Pedro María Ureña', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(270, 'Rafael Urdaneta', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(271, 'Samuel Darío Maldonado', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(272, 'San Cristóbal', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(273, 'Seboruco', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(274, 'Simón Rodríguez', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(275, 'Sucre', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(276, 'Torbes', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(277, 'Uribante', 19, '2019-12-21 18:47:04', '2019-12-21 18:47:04'),
	(278, 'San Judas Tadeo', 19, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(279, 'Andrés Bello', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(280, 'Boconó', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(281, 'Bolívar', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(282, 'Candelaria', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(283, 'Carache', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(284, 'Escuque', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(285, 'José Felipe Márquez Cañizalez', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(286, 'Juan Vicente Campos Elías', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(287, 'La Ceiba', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(288, 'Miranda', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(289, 'Monte Carmelo', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(290, 'Motatán', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(291, 'Pampán', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(292, 'Pampanito', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(293, 'Rafael Rangel', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(294, 'San Rafael de Carvajal', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(295, 'Sucre', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(296, 'Trujillo', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(297, 'Urdaneta', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(298, 'Valera', 20, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(299, 'Vargas', 21, '2019-12-21 18:47:05', '2019-12-21 18:47:05'),
	(300, 'Arístides Bastidas', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(301, 'Bolívar', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(302, 'Bruzual', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(303, 'Cocorote', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(304, 'Independencia', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(305, 'José Antonio Páez', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(306, 'La Trinidad', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(307, 'Manuel Monge', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(308, 'Nirgua', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(309, 'Peña', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(310, 'San Felipe', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(311, 'Sucre', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(312, 'Urachiche', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(313, 'José Joaquín Veroes', 22, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(314, 'Almirante Padilla', 23, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(315, 'Baralt', 23, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(316, 'Cabimas', 23, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(317, 'Catatumbo', 23, '2019-12-21 18:47:06', '2019-12-21 18:47:06'),
	(318, 'Colón', 23, '2019-12-21 18:47:07', '2019-12-21 18:47:07'),
	(319, 'Francisco Javier Pulgar', 23, '2019-12-21 18:47:07', '2019-12-21 18:47:07'),
	(320, 'Páez', 23, '2019-12-21 18:47:07', '2019-12-21 18:47:07'),
	(321, 'Jesús Enrique Losada', 23, '2019-12-21 18:47:07', '2019-12-21 18:47:07'),
	(322, 'Jesús María Semprún', 23, '2019-12-21 18:47:07', '2019-12-21 18:47:07'),
	(323, 'La Cañada de Urdaneta', 23, '2019-12-21 18:47:07', '2019-12-21 18:47:07'),
	(324, 'Lagunillas', 23, '2019-12-21 18:47:07', '2019-12-21 18:47:07'),
	(325, 'Machiques de Perijá', 23, '2019-12-21 18:47:07', '2019-12-21 18:47:07'),
	(326, 'Mara', 23, '2019-12-21 18:47:07', '2019-12-21 18:47:07'),
	(327, 'Maracaibo', 23, '2019-12-21 18:47:08', '2019-12-21 18:47:08'),
	(328, 'Miranda', 23, '2019-12-21 18:47:08', '2019-12-21 18:47:08'),
	(329, 'Rosario de Perijá', 23, '2019-12-21 18:47:08', '2019-12-21 18:47:08'),
	(330, 'San Francisco', 23, '2019-12-21 18:47:08', '2019-12-21 18:47:08'),
	(331, 'Santa Rita', 23, '2019-12-21 18:47:08', '2019-12-21 18:47:08'),
	(332, 'Simón Bolívar', 23, '2019-12-21 18:47:08', '2019-12-21 18:47:08'),
	(333, 'Sucre', 23, '2019-12-21 18:47:08', '2019-12-21 18:47:08'),
	(334, 'Valmore Rodríguez', 23, '2019-12-21 18:47:08', '2019-12-21 18:47:08'),
	(335, 'Libertador', 24, '2019-12-21 18:47:08', '2019-12-21 18:47:08');
/*!40000 ALTER TABLE `towns` ENABLE KEYS */;

-- Volcando estructura para tabla ovinosdb.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identification` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `predio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomenclature` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomenclatureSuggest1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomenclatureSuggest2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomenclatureSuggest3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('aprobe','reprobe') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('superuser','admin','partner') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'partner',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `payed` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `town_id` int(11) DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `zip` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla ovinosdb.users: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `identification`, `phone`, `company`, `predio`, `nomenclature`, `nomenclatureSuggest1`, `nomenclatureSuggest2`, `nomenclatureSuggest3`, `status`, `role`, `remember_token`, `created_at`, `updated_at`, `completed`, `payed`, `country_id`, `state_id`, `town_id`, `city`, `address`, `zip`) VALUES
	(1, 'Administrador', 'admin@ovinos.com', NULL, '$2y$10$ckjF4Kes59r4u2jj.mYayuY73im8l1SaMAjqMjnIwDSQEeghPWN7.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', NULL, '2019-12-21 18:46:43', '2019-12-21 18:46:43', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Super User', 'superuser@ovinos.com', NULL, '$2y$10$W3S3HM5D7PYuTafnBh4qBO3djaz63iviR7H4R5nvluSVErAUU3CDC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'superuser', NULL, '2019-12-21 18:46:43', '2019-12-21 18:46:43', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Asociado', 'partner@ovinos.com', NULL, '$2y$10$01gDxF82AvkQXervmigoUe9MdhqzvXiNK9XyP12T5GBj6AqTkR3SS', NULL, NULL, NULL, NULL, 'AA11', NULL, NULL, NULL, 'aprobe', 'partner', NULL, '2019-12-21 18:46:44', '2019-12-21 18:46:44', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
