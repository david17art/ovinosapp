-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: ovinosDb
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `animals`
--

DROP TABLE IF EXISTS `animals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('macho','hembra') COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `weight` double(8,2) NOT NULL,
  `parto` enum('simple','double','triple','multiple') COLLATE utf8mb4_unicode_ci NOT NULL,
  `dad_id` int(11) DEFAULT NULL,
  `mom_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `race_id` int(11) NOT NULL,
  `tatto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('revision','aprobado','rechazado') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'revision',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `criador` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generation` int(11) DEFAULT NULL,
  `otorgamiento` date DEFAULT NULL,
  `clasificador` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `animals_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animals`
--

LOCK TABLES `animals` WRITE;
/*!40000 ALTER TABLE `animals` DISABLE KEYS */;
INSERT INTO `animals` VALUES (1,'Padre 1','macho','2017-08-08',10.00,'simple',NULL,NULL,NULL,1,'P1','aprobado','2020-01-15 09:35:04','2020-01-15 09:35:04',0,NULL,NULL,NULL,NULL,NULL,0),(2,'Padre 2','macho','2017-08-08',10.00,'simple',NULL,NULL,NULL,2,'P1','aprobado','2020-01-15 09:35:04','2020-01-15 09:35:04',0,NULL,NULL,NULL,NULL,NULL,0),(3,'Madre 1','hembra','2017-08-08',10.00,'simple',NULL,NULL,NULL,2,'M1','aprobado','2020-01-15 09:35:04','2020-01-15 09:35:04',0,NULL,NULL,NULL,NULL,NULL,0),(4,'Madre 2','hembra','2017-08-08',10.00,'simple',NULL,NULL,NULL,2,'M2','aprobado','2020-01-15 09:35:04','2020-01-15 09:35:04',0,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `animals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banks`
--

LOCK TABLES `banks` WRITE;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;
INSERT INTO `banks` VALUES (1,156,'100%BANCO','2020-01-24 22:29:52','2020-01-24 22:29:52'),(2,196,'ABN-AMRO-BANK','2020-01-24 22:29:52','2020-01-24 22:29:52'),(3,172,'BANCAMIGA-BANCO-MICROFINANCIERO','2020-01-24 22:29:52','2020-01-24 22:29:52'),(4,171,'BANCO-ACTIVO-BANCO-COMERCIAL','2020-01-24 22:29:53','2020-01-24 22:29:53'),(5,166,'BANCO-AGRICOLA','2020-01-24 22:29:53','2020-01-24 22:29:53'),(6,175,'BANCO-BICENTENARIO','2020-01-24 22:29:53','2020-01-24 22:29:53'),(7,128,'BANCO-CARONI-BANCO-UNIVERSAL','2020-01-24 22:29:53','2020-01-24 22:29:53'),(8,164,'BANCO-DE-DESARROLLO-DEL-MICROEMPRESARIO','2020-01-24 22:29:53','2020-01-24 22:29:53'),(9,102,'BANCO-DE-VENEZUELA','2020-01-24 22:29:53','2020-01-24 22:29:53'),(10,114,'BANCO-DEL-CARIBE','2020-01-24 22:29:53','2020-01-24 22:29:53'),(11,149,'BANCO-DEL-PUEBLO-SOBERANO','2020-01-24 22:29:53','2020-01-24 22:29:53'),(12,163,'BANCO-DEL-TESORO','2020-01-24 22:29:53','2020-01-24 22:29:53'),(13,176,'BANCO-ESPIRITO-SANTO','2020-01-24 22:29:53','2020-01-24 22:29:53'),(14,115,'BANCO-EXTERIOR','2020-01-24 22:29:53','2020-01-24 22:29:53'),(15,3,'BANCO-INDUSTRIAL-DE-VENEZUELA','2020-01-24 22:29:53','2020-01-24 22:29:53'),(16,173,'BANCO-INTERNACIONAL-DE-DESARROLLO','2020-01-24 22:29:53','2020-01-24 22:29:53'),(17,105,'BANCO-MERCANTIL','2020-01-24 22:29:53','2020-01-24 22:29:53'),(18,191,'BANCO-NACIONAL-DE-CREDITO','2020-01-24 22:29:53','2020-01-24 22:29:53'),(19,116,'BANCO-OCCIDENTAL-DE-DESCUENTO','2020-01-24 22:29:53','2020-01-24 22:29:53'),(20,138,'BANCO-PLAZA','2020-01-24 22:29:54','2020-01-24 22:29:54'),(21,108,'BANCO-PROVINCIAL-BBVA','2020-01-24 22:29:54','2020-01-24 22:29:54'),(22,104,'BANCO-VENEZOLANO-DE-CREDITO','2020-01-24 22:29:54','2020-01-24 22:29:54'),(23,168,'BANCRECER-BANCO-DE-DESARROLLO','2020-01-24 22:29:54','2020-01-24 22:29:54'),(24,134,'BANESCO-BANCO-UNIVERSAL','2020-01-24 22:29:54','2020-01-24 22:29:54'),(25,177,'BANFANB','2020-01-24 22:29:54','2020-01-24 22:29:54'),(26,146,'BANGENTE','2020-01-24 22:29:54','2020-01-24 22:29:54'),(27,174,'BANPLUS-BANCO-COMERCIAL','2020-01-24 22:29:54','2020-01-24 22:29:54'),(28,190,'CITIBANK','2020-01-24 22:29:54','2020-01-24 22:29:54'),(29,121,'CORP-BANCA','2020-01-24 22:29:54','2020-01-24 22:29:54'),(30,157,'DELSUR-BANCO-UNIVERSAL','2020-01-24 22:29:54','2020-01-24 22:29:54'),(31,151,'FONDO-COMUN','2020-01-24 22:29:54','2020-01-24 22:29:54'),(32,601,'INSTITUTO-MUNICIPAL-DE-CREDITO-POPULAR','2020-01-24 22:29:54','2020-01-24 22:29:54'),(33,169,'MIBANCO-BANCO-DE-DESARROLLO','2020-01-24 22:29:54','2020-01-24 22:29:54'),(34,137,'SOFITASA','2020-01-24 22:29:54','2020-01-24 22:29:54');
/*!40000 ALTER TABLE `banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Venezuela','2020-01-15 09:35:04','2020-01-15 09:35:04');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_05_062622_create_nomenclatures_table',1),(5,'2019_12_11_061443_create_races_table',1),(6,'2019_12_13_034943_create_animals_table',1),(7,'2019_12_20_035942_add_completed_member_on_table_users',1),(8,'2019_12_20_040254_add_completed_animal_on_table_animals',1),(9,'2019_12_20_041437_create_countries_table',1),(10,'2019_12_20_041449_create_states_table',1),(11,'2019_12_20_041501_create_towns_table',1),(12,'2019_12_21_042634_create_payments_table',1),(14,'2020_01_24_181556_create_bank_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nomenclatures`
--

DROP TABLE IF EXISTS `nomenclatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomenclatures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `selected` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nomenclatures_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nomenclatures`
--

LOCK TABLES `nomenclatures` WRITE;
/*!40000 ALTER TABLE `nomenclatures` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomenclatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `refer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(12,2) NOT NULL,
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `animal_id` int(11) DEFAULT NULL,
  `status` enum('aprobe','reprobe','revision') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'revision',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `races`
--

DROP TABLE IF EXISTS `races`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `races` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `races`
--

LOCK TABLES `races` WRITE;
/*!40000 ALTER TABLE `races` DISABLE KEYS */;
INSERT INTO `races` VALUES (1,'Raza 1','2020-01-15 09:35:04','2020-01-15 09:35:04'),(2,'Raza 2','2020-01-15 09:35:04','2020-01-15 09:35:04'),(3,'Raza 3','2020-01-15 09:35:04','2020-01-15 09:35:04');
/*!40000 ALTER TABLE `races` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `states` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` VALUES (1,1,'Amazonas','2020-01-15 09:35:04','2020-01-15 09:35:04'),(2,1,'Anzoátegui','2020-01-15 09:35:04','2020-01-15 09:35:04'),(3,1,'Apure','2020-01-15 09:35:04','2020-01-15 09:35:04'),(4,1,'Aragua','2020-01-15 09:35:05','2020-01-15 09:35:05'),(5,1,'Barinas','2020-01-15 09:35:05','2020-01-15 09:35:05'),(6,1,'Bolívar','2020-01-15 09:35:05','2020-01-15 09:35:05'),(7,1,'Carabobo','2020-01-15 09:35:05','2020-01-15 09:35:05'),(8,1,'Cojedes','2020-01-15 09:35:05','2020-01-15 09:35:05'),(9,1,'Delta Amacuro','2020-01-15 09:35:05','2020-01-15 09:35:05'),(10,1,'Falcón','2020-01-15 09:35:05','2020-01-15 09:35:05'),(11,1,'Guárico','2020-01-15 09:35:05','2020-01-15 09:35:05'),(12,1,'Lara','2020-01-15 09:35:05','2020-01-15 09:35:05'),(13,1,'Mérida','2020-01-15 09:35:05','2020-01-15 09:35:05'),(14,1,'Miranda','2020-01-15 09:35:05','2020-01-15 09:35:05'),(15,1,'Monagas','2020-01-15 09:35:05','2020-01-15 09:35:05'),(16,1,'Nueva Esparta','2020-01-15 09:35:05','2020-01-15 09:35:05'),(17,1,'Portuguesa','2020-01-15 09:35:05','2020-01-15 09:35:05'),(18,1,'Sucre','2020-01-15 09:35:05','2020-01-15 09:35:05'),(19,1,'Táchira','2020-01-15 09:35:05','2020-01-15 09:35:05'),(20,1,'Trujillo','2020-01-15 09:35:05','2020-01-15 09:35:05'),(21,1,'Vargas','2020-01-15 09:35:06','2020-01-15 09:35:06'),(22,1,'Yaracuy','2020-01-15 09:35:06','2020-01-15 09:35:06'),(23,1,'Zulia','2020-01-15 09:35:06','2020-01-15 09:35:06'),(24,1,'Distrito Capital','2020-01-15 09:35:06','2020-01-15 09:35:06'),(25,1,'Dependencias Federales','2020-01-15 09:35:06','2020-01-15 09:35:06');
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `towns`
--

DROP TABLE IF EXISTS `towns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `towns` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `towns`
--

LOCK TABLES `towns` WRITE;
/*!40000 ALTER TABLE `towns` DISABLE KEYS */;
INSERT INTO `towns` VALUES (1,'Alto Orinoco',1,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(2,'Atabapo',1,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(3,'Atures',1,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(4,'Autana',1,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(5,'Manapiare',1,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(6,'Maroa',1,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(7,'Río Negro',1,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(8,'Anaco',2,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(9,'Aragua',2,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(10,'Manuel Ezequiel Bruzual',2,'2020-01-15 09:35:06','2020-01-15 09:35:06'),(11,'Diego Bautista Urbaneja',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(12,'Fernando Peñalver',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(13,'Francisco Del Carmen Carvajal',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(14,'General Sir Arthur McGregor',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(15,'Guanta',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(16,'Independencia',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(17,'José Gregorio Monagas',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(18,'Juan Antonio Sotillo',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(19,'Juan Manuel Cajigal',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(20,'Libertad',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(21,'Francisco de Miranda',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(22,'Pedro María Freites',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(23,'Píritu',2,'2020-01-15 09:35:07','2020-01-15 09:35:07'),(24,'San José de Guanipa',2,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(25,'San Juan de Capistrano',2,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(26,'Santa Ana',2,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(27,'Simón Bolívar',2,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(28,'Simón Rodríguez',2,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(29,'Achaguas',3,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(30,'Biruaca',3,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(31,'Muñóz',3,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(32,'Páez',3,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(33,'Pedro Camejo',3,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(34,'Rómulo Gallegos',3,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(35,'San Fernando',3,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(36,'Atanasio Girardot',4,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(37,'Bolívar',4,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(38,'Camatagua',4,'2020-01-15 09:35:08','2020-01-15 09:35:08'),(39,'Francisco Linares Alcántara',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(40,'José Ángel Lamas',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(41,'José Félix Ribas',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(42,'José Rafael Revenga',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(43,'Libertador',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(44,'Mario Briceño Iragorry',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(45,'Ocumare de la Costa de Oro',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(46,'San Casimiro',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(47,'San Sebastián',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(48,'Santiago Mariño',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(49,'Santos Michelena',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(50,'Sucre',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(51,'Tovar',4,'2020-01-15 09:35:09','2020-01-15 09:35:09'),(52,'Urdaneta',4,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(53,'Zamora',4,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(54,'Alberto Arvelo Torrealba',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(55,'Andrés Eloy Blanco',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(56,'Antonio José de Sucre',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(57,'Arismendi',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(58,'Barinas',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(59,'Bolívar',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(60,'Cruz Paredes',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(61,'Ezequiel Zamora',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(62,'Obispos',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(63,'Pedraza',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(64,'Rojas',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(65,'Sosa',5,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(66,'Caroní',6,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(67,'Cedeño',6,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(68,'El Callao',6,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(69,'Gran Sabana',6,'2020-01-15 09:35:10','2020-01-15 09:35:10'),(70,'Heres',6,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(71,'Piar',6,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(72,'Angostura (Raúl Leoni)',6,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(73,'Roscio',6,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(74,'Sifontes',6,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(75,'Sucre',6,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(76,'Padre Pedro Chien',6,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(77,'Bejuma',7,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(78,'Carlos Arvelo',7,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(79,'Diego Ibarra',7,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(80,'Guacara',7,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(81,'Juan José Mora',7,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(82,'Libertador',7,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(83,'Los Guayos',7,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(84,'Miranda',7,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(85,'Montalbán',7,'2020-01-15 09:35:11','2020-01-15 09:35:11'),(86,'Naguanagua',7,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(87,'Puerto Cabello',7,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(88,'San Diego',7,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(89,'San Joaquín',7,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(90,'Valencia',7,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(91,'Anzoátegui',8,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(92,'Tinaquillo',8,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(93,'Girardot',8,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(94,'Lima Blanco',8,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(95,'Pao de San Juan Bautista',8,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(96,'Ricaurte',8,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(97,'Rómulo Gallegos',8,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(98,'San Carlos',8,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(99,'Tinaco',8,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(100,'Antonio Díaz',9,'2020-01-15 09:35:12','2020-01-15 09:35:12'),(101,'Casacoima',9,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(102,'Pedernales',9,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(103,'Tucupita',9,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(104,'Acosta',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(105,'Bolívar',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(106,'Buchivacoa',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(107,'Cacique Manaure',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(108,'Carirubana',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(109,'Colina',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(110,'Dabajuro',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(111,'Democracia',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(112,'Falcón',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(113,'Federación',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(114,'Jacura',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(115,'José Laurencio Silva',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(116,'Los Taques',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(117,'Mauroa',10,'2020-01-15 09:35:13','2020-01-15 09:35:13'),(118,'Miranda',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(119,'Monseñor Iturriza',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(120,'Palmasola',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(121,'Petit',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(122,'Píritu',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(123,'San Francisco',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(124,'Sucre',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(125,'Tocópero',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(126,'Unión',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(127,'Urumaco',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(128,'Zamora',10,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(129,'Camaguán',11,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(130,'Chaguaramas',11,'2020-01-15 09:35:14','2020-01-15 09:35:14'),(131,'El Socorro',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(132,'José Félix Ribas',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(133,'José Tadeo Monagas',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(134,'Juan Germán Roscio',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(135,'Julián Mellado',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(136,'Las Mercedes',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(137,'Leonardo Infante',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(138,'Pedro Zaraza',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(139,'Ortíz',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(140,'San Gerónimo de Guayabal',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(141,'San José de Guaribe',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(142,'Santa María de Ipire',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(143,'Sebastián Francisco de Miranda',11,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(144,'Andrés Eloy Blanco',12,'2020-01-15 09:35:15','2020-01-15 09:35:15'),(145,'Crespo',12,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(146,'Iribarren',12,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(147,'Jiménez',12,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(148,'Morán',12,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(149,'Palavecino',12,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(150,'Simón Planas',12,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(151,'Torres',12,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(152,'Urdaneta',12,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(153,'Alberto Adriani',13,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(154,'Andrés Bello',13,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(155,'Antonio Pinto Salinas',13,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(156,'Aricagua',13,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(157,'Arzobispo Chacón',13,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(158,'Campo Elías',13,'2020-01-15 09:35:16','2020-01-15 09:35:16'),(159,'Caracciolo Parra Olmedo',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(160,'Cardenal Quintero',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(161,'Guaraque',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(162,'Julio César Salas',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(163,'Justo Briceño',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(164,'Libertador',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(165,'Miranda',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(166,'Obispo Ramos de Lora',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(167,'Padre Noguera',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(168,'Pueblo Llano',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(169,'Rangel',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(170,'Rivas Dávila',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(171,'Santos Marquina',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(172,'Sucre',13,'2020-01-15 09:35:17','2020-01-15 09:35:17'),(173,'Tovar',13,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(174,'Tulio Febres Cordero',13,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(175,'Zea',13,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(176,'Acevedo',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(177,'Andrés Bello',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(178,'Baruta',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(179,'Brión',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(180,'Buroz',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(181,'Carrizal',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(182,'Chacao',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(183,'Cristóbal Rojas',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(184,'El Hatillo',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(185,'Guaicaipuro',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(186,'Independencia',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(187,'Lander',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(188,'Los Salias',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(189,'Páez',14,'2020-01-15 09:35:18','2020-01-15 09:35:18'),(190,'Paz Castillo',14,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(191,'Pedro Gual',14,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(192,'Plaza',14,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(193,'Simón Bolívar',14,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(194,'Sucre',14,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(195,'Urdaneta',14,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(196,'Zamora',14,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(197,'Acosta',15,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(198,'Aguasay',15,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(199,'Bolívar',15,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(200,'Caripe',15,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(201,'Cedeño',15,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(202,'Ezequiel Zamora',15,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(203,'Libertador',15,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(204,'Maturín',15,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(205,'Piar',15,'2020-01-15 09:35:19','2020-01-15 09:35:19'),(206,'Punceres',15,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(207,'Santa Bárbara',15,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(208,'Sotillo',15,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(209,'Uracoa',15,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(210,'Antolín del Campo',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(211,'Arismendi',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(212,'García',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(213,'Gómez',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(214,'Maneiro',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(215,'Marcano',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(216,'Mariño',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(217,'Península de Macanao',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(218,'Tubores',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(219,'Villalba',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(220,'Díaz',16,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(221,'Agua Blanca',17,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(222,'Araure',17,'2020-01-15 09:35:20','2020-01-15 09:35:20'),(223,'Esteller',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(224,'Guanare',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(225,'Guanarito',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(226,'Monseñor José Vicente de Unda',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(227,'Ospino',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(228,'Páez',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(229,'Papelón',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(230,'San Genaro de Boconoíto',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(231,'San Rafael de Onoto',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(232,'Santa Rosalía',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(233,'Sucre',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(234,'Turén',17,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(235,'Andrés Eloy Blanco',18,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(236,'Andrés Mata',18,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(237,'Arismendi',18,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(238,'Benítez',18,'2020-01-15 09:35:21','2020-01-15 09:35:21'),(239,'Bermúdez',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(240,'Bolívar',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(241,'Cajigal',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(242,'Cruz Salmerón Acosta',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(243,'Libertador',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(244,'Mariño',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(245,'Mejía',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(246,'Montes',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(247,'Ribero',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(248,'Sucre',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(249,'Valdéz',18,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(250,'Andrés Bello',19,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(251,'Antonio Rómulo Costa',19,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(252,'Ayacucho',19,'2020-01-15 09:35:22','2020-01-15 09:35:22'),(253,'Bolívar',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(254,'Cárdenas',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(255,'Córdoba',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(256,'Fernández Feo',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(257,'Francisco de Miranda',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(258,'García de Hevia',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(259,'Guásimos',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(260,'Independencia',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(261,'Jáuregui',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(262,'José María Vargas',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(263,'Junín',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(264,'Libertad',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(265,'Libertador',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(266,'Lobatera',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(267,'Michelena',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(268,'Panamericano',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(269,'Pedro María Ureña',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(270,'Rafael Urdaneta',19,'2020-01-15 09:35:23','2020-01-15 09:35:23'),(271,'Samuel Darío Maldonado',19,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(272,'San Cristóbal',19,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(273,'Seboruco',19,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(274,'Simón Rodríguez',19,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(275,'Sucre',19,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(276,'Torbes',19,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(277,'Uribante',19,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(278,'San Judas Tadeo',19,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(279,'Andrés Bello',20,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(280,'Boconó',20,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(281,'Bolívar',20,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(282,'Candelaria',20,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(283,'Carache',20,'2020-01-15 09:35:24','2020-01-15 09:35:24'),(284,'Escuque',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(285,'José Felipe Márquez Cañizalez',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(286,'Juan Vicente Campos Elías',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(287,'La Ceiba',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(288,'Miranda',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(289,'Monte Carmelo',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(290,'Motatán',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(291,'Pampán',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(292,'Pampanito',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(293,'Rafael Rangel',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(294,'San Rafael de Carvajal',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(295,'Sucre',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(296,'Trujillo',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(297,'Urdaneta',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(298,'Valera',20,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(299,'Vargas',21,'2020-01-15 09:35:25','2020-01-15 09:35:25'),(300,'Arístides Bastidas',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(301,'Bolívar',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(302,'Bruzual',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(303,'Cocorote',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(304,'Independencia',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(305,'José Antonio Páez',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(306,'La Trinidad',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(307,'Manuel Monge',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(308,'Nirgua',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(309,'Peña',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(310,'San Felipe',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(311,'Sucre',22,'2020-01-15 09:35:26','2020-01-15 09:35:26'),(312,'Urachiche',22,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(313,'José Joaquín Veroes',22,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(314,'Almirante Padilla',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(315,'Baralt',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(316,'Cabimas',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(317,'Catatumbo',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(318,'Colón',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(319,'Francisco Javier Pulgar',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(320,'Páez',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(321,'Jesús Enrique Losada',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(322,'Jesús María Semprún',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(323,'La Cañada de Urdaneta',23,'2020-01-15 09:35:27','2020-01-15 09:35:27'),(324,'Lagunillas',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(325,'Machiques de Perijá',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(326,'Mara',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(327,'Maracaibo',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(328,'Miranda',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(329,'Rosario de Perijá',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(330,'San Francisco',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(331,'Santa Rita',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(332,'Simón Bolívar',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(333,'Sucre',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(334,'Valmore Rodríguez',23,'2020-01-15 09:35:28','2020-01-15 09:35:28'),(335,'Libertador',24,'2020-01-15 09:35:28','2020-01-15 09:35:28');
/*!40000 ALTER TABLE `towns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identification` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `predio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomenclature` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomenclatureSuggest1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomenclatureSuggest2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomenclatureSuggest3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('aprobe','reprobe') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('superuser','admin','partner') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'partner',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `payed` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `town_id` int(11) DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `zip` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrador','admin@ovinos.com',NULL,'$2y$10$fajSFTm/JqVcgnwnzD3AX.g8AbHbtqm0kIhX/6WQiQUS3wrJoDFFW',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin',NULL,'2020-01-15 09:35:03','2020-01-15 09:35:03',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Super User','superuser@ovinos.com',NULL,'$2y$10$//nqv.p7aIxW6B0mjVByZ.td1o8j2JuMH10.NRu6TBXgpax6G2eOy',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'superuser',NULL,'2020-01-15 09:35:04','2020-01-15 09:35:04',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Asociado','partner@ovinos.com',NULL,'$2y$10$aN/SXF3eXEzf9xYrIGvtueE7UfuxNroE5KchbeiV8bVdYUbq7MVkO',NULL,NULL,NULL,NULL,'AA11',NULL,NULL,NULL,'aprobe','partner',NULL,'2020-01-15 09:35:04','2020-01-15 09:35:04',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Prof. Shayna Ernser I','emily.abernathy@vandervort.com',NULL,'$2y$10$4KwGpkrg7LEjex85CF9AweaXdTlrS7AbbAZEls9PpwzLxY3J/.YXS','37019170','+1.813.953.8569','Ondricka and Sons','Inc',NULL,'lb64','mm81','fs34','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Dr. Nicolas Lockman','thayes@gmail.com',NULL,'$2y$10$x6.tqrK4ixxL1kCrOlK3A.aR5pay0JKlsUYSn1.NIAGkwRWMqqDQq','44262062','925.266.0600 x52412','Koch Group','Inc',NULL,'zk86','qn25','sy13','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Ronaldo Cummerata','keira99@reichel.net',NULL,'$2y$10$yaPTK3qMscPH37T5wcK2weXh9SjWllNloiP2ofSvi7pg2VQMSUH3G','96854839','1-913-499-2737 x546','Bergnaum LLC','Inc',NULL,'ot28','gb78','ul19','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Willard Lowe','makenzie.collier@reynolds.biz',NULL,'$2y$10$ykRyleyaTlntmhnFQh3.xee1NDrj3QyG.9oOG7brlXQUutGX2CUQC','88895771','(461) 753-1319','Luettgen Group','LLC',NULL,'mg39','xm52','nd52','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Ebba Schumm','mgottlieb@okon.com',NULL,'$2y$10$PtkylY.7Ms1dy9mRrlBzFeICAzqCfx7sb/UDxuvRL6KkCBDLutDva','36486244','(782) 948-1685 x0428','Lesch-Schiller','LLC',NULL,'cr52','hg79','of11','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Mr. Liam Hayes','jwalter@yahoo.com',NULL,'$2y$10$TFIwzyJCqugVDF2Azgu.yeOThuscvk4phg3RxYvtGD5r7bHqEsSce','34288379','450.767.2049 x405','Doyle-Raynor','Ltd',NULL,'ri49','ls87','mz54','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(10,'Frances Koch','sauer.marcelle@hotmail.com',NULL,'$2y$10$KaRh85VfULPMBWdXhBrhuunf9rP6ABf8fuq3Hj.tdO0kUBBaPmgD.','83780167','+15937987726','Gislason, Thiel and Glover','Group',NULL,'hm13','dk29','bz65','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(11,'Mrs. Ava Kuhic','okey90@hotmail.com',NULL,'$2y$10$mLhFGyVMwJFxN5bmdjPU9O5E6.hyQLXLNZy0ugzuX4Dqc3m59b9cy','74572712','1-494-806-1537 x686','Konopelski-Mueller','Group',NULL,'ak58','op46','yt71','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(12,'Maurine Weimann','dedric.corwin@hotmail.com',NULL,'$2y$10$/VfoyhlGb78bay4l0b508enBaj/ljsIVLv4Fpdz2BtLCjoBuj7/22','52097980','+1.270.977.4912','Ward Ltd','LLC',NULL,'jh28','td72','ey65','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(13,'Hunter Strosin','zgerhold@yahoo.com',NULL,'$2y$10$mMIGAX4ZiNehSB9GUqK9BOS5iOp9skP/x3bKk2EZSmzDsClvlJS0y','20312508','994.649.4762 x21113','Strosin Ltd','Inc',NULL,'ff11','vo71','oy94','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(14,'Easton Kuhn V','oberbrunner.raina@brekke.com',NULL,'$2y$10$Lvt3a76H.GWyVgIZCRVJleOVo4qGCPb8xIZPw4eECwtfTEljpKTMW','91200454','478-465-2490','Kohler Group','and Sons',NULL,'yy95','bv21','dq81','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(15,'Mrs. Teagan Swaniawski','bhuels@kutch.org',NULL,'$2y$10$dMFB1xw1y6OOPAKfZJospOABStsFufPA5rDED0L1rcDCrFwT/krom','96329586','949-648-7211 x3848','Lubowitz-Graham','PLC',NULL,'sf18','fl17','mg14','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(16,'Antonina Roberts','iwindler@hotmail.com',NULL,'$2y$10$EAfL2ERtT0QY4pJcN0LF/OFRdmKof3g/CVwYw7K8ILfa/XMVVDQ9G','36086946','+1-408-305-6004','Bashirian-Auer','and Sons',NULL,'lw63','ar54','bn58','reprobe','partner',NULL,'2020-01-24 22:51:08','2020-01-24 22:51:08',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(17,'Greyson Feeney','zoey84@yahoo.com',NULL,'$2y$10$piJxZjCA5FOEE2jZkxQh6.E7PioelwilhuViwYCbjWymfVSfmxvfO','50248831','(369) 660-9075 x0382','Kunde Ltd','and Sons',NULL,'kn37','wd88','wy43','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(18,'Janessa Jacobs','colt.hartmann@bauch.com',NULL,'$2y$10$8kJaCZMgySasBzwJHsW1D.Dv5FuXotnPVZwZAc5wzYW5OJN2vAgK2','15749058','1-942-832-0867','Pouros, Pouros and Dicki','and Sons',NULL,'zz28','tv17','kl16','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(19,'Cordia Larkin V','jerome.altenwerth@stroman.biz',NULL,'$2y$10$kQFEnGLuThF0nTREqtTsyueffrTPjW5JvE.o/Wu2gncPJ/I18ALQy','53811612','1-284-884-1200 x17883','Reichert, Kulas and Gulgowski','PLC',NULL,'jj31','im45','vh96','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(20,'Mr. Otis Bogan','malachi.jacobs@beatty.org',NULL,'$2y$10$MaQfy3kwKiTjFcpISN6Dx.UHyyqLmsuaMDNWPyqqB60fXgMiKZaTq','43494617','317-810-3141','Larson-Thompson','Group',NULL,'vv92','wb16','fs67','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(21,'Erich Effertz','elnora.collier@gmail.com',NULL,'$2y$10$zGRjvnnE8gCj3/6EBzUi6u31uKkfBHtRvgrmslOg4BVvqRmn6E8qq','54306492','(697) 748-3702 x9689','Schowalter-Bauch','Group',NULL,'ov94','vr43','el77','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(22,'Prof. Colt Effertz MD','maggie88@hotmail.com',NULL,'$2y$10$TzQ5tBqRlFg.Y0MWDH2jjOhDFe1dMOAZFHlUliL89TZt8GnkR8WDC','96399163','+18915878721','Hirthe-Adams','Inc',NULL,'yh66','nf74','hg27','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(23,'Lucy Bogisich Jr.','hector01@jaskolski.com',NULL,'$2y$10$jxHkbhJdSY7QcVFQLJz7o.Wn/2D4DgdUrvPipfMnXI63QoGlE5ZAm','96168638','787-347-5816 x474','Price Ltd','LLC',NULL,'un45','rc46','wt61','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(24,'Dr. Rozella Hagenes DVM','philip61@schroeder.com',NULL,'$2y$10$QI2/f13OsKPx6rwF5bwE/OzrLDBDYrsMP5MZZ/xpgqA8X2qGn6AWS','14930821','+1-849-454-9975','Padberg Inc','and Sons',NULL,'cv42','qg18','pw47','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(25,'Christophe Wisozk','alexie.stiedemann@tremblay.com',NULL,'$2y$10$uccWRJxLbcbc6RkRu5xL6ebG1TEB/YqmWEwTUirNn0jExqXbx1mce','14994168','+18319719253','Konopelski-Jacobi','and Sons',NULL,'vl81','dh12','op31','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(26,'Justus Bednar','eriberto44@gmail.com',NULL,'$2y$10$SWqJ3Tt1FQVa5g9OKNtzse5XhYx8oyzfCyAhWJUeqE6j4VfWeiHmm','78788387','+1.470.480.2153','Adams-Weissnat','and Sons',NULL,'lm29','nb52','dt26','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(27,'Mr. Brady Schuster III','ykoepp@gmail.com',NULL,'$2y$10$0p/.DCQgsE2ladXLOA9/iejh5voPMLVZQqhKyKQSbvfyVfNGarjvu','43661504','267.577.2538 x92238','Wolff-Turner','and Sons',NULL,'bo87','rh44','st45','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(28,'Eloise White','sporer.linwood@gmail.com',NULL,'$2y$10$6x134dXqgf50pZiq0MzUAeukQk.eVhuqO5CnSJmDky4fZ5L4BFapu','23191014','+1.360.223.2577','Bahringer Ltd','PLC',NULL,'ja47','ys68','fl52','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(29,'Kurt O\'Reilly Jr.','frida72@larson.biz',NULL,'$2y$10$i9r.efVzX005p6B4r6D3Fe.6mjrYJ7tyza2w9PyCek.FbYaE0ZRQK','96440008','695.821.3409 x61900','Labadie, Nolan and Leuschke','PLC',NULL,'bj66','ui83','xr15','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(30,'Enrico Feeney','tremayne.hagenes@gmail.com',NULL,'$2y$10$iCZ9PB4rOXQCO0/hkm8fjelOABdhOs0IY0dV.rbrP9zoVx/Qb9T4y','72683496','552.575.8985 x08827','Bergnaum, Kautzer and Pagac','Group',NULL,'qi83','ho28','ys14','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(31,'Dr. Kailyn Ernser III','kovacek.dario@gulgowski.org',NULL,'$2y$10$3hrn3zFRGFVp5/WR3uGyxuxSyUuI10f5kzqaN/R4zIG77VVDrGZoK','18710773','569.723.0665','Pacocha-Pollich','and Sons',NULL,'rh52','ca99','ca73','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(32,'Demarco Yost','muriel.predovic@gmail.com',NULL,'$2y$10$ZMp47loEiaPTxEEUDgh/9O8PgnIJHNjp.A6F.IDfh2HwFv4vJCKRG','96917809','808-575-7318','Hodkiewicz Group','Inc',NULL,'an16','iy52','yc38','reprobe','partner',NULL,'2020-01-24 22:51:09','2020-01-24 22:51:09',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(33,'Desiree Sporer','ashtyn.nikolaus@baumbach.info',NULL,'$2y$10$tLHEKX0z1T8JsKEUVybu3OKEAkU3mtX4tSeJgv2JfRYQcb5itqngC','97128433','1-807-208-4532 x3329','Quitzon-Terry','Group',NULL,'ug87','in56','ot64','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(34,'Ms. Marcia Sanford','hking@oconner.com',NULL,'$2y$10$.EDv5CB34eIvnApHb1TFZ.WqAp0DWohPMI8jHiv7ICcXonT3Rptdi','729967','(746) 493-9480','Pfeffer Group','Ltd',NULL,'wo35','cr85','mv94','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(35,'Sam Lind','eldridge.heidenreich@hotmail.com',NULL,'$2y$10$fQ37GW0enUiHmjg30JCdZe6w4jD6/gAi4YaGNUcPT7Prk5bNMR4Km','98845679','596.434.9506 x40007','O\'Keefe-Halvorson','Ltd',NULL,'nw59','tv18','nj98','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(36,'Mrs. Gladys Bergnaum','stewart.lehner@gmail.com',NULL,'$2y$10$yHn/OaZVIkW2zjMNoS9wbeYnLH5u/ei4sWSrXe0RyztLFaGa39rLa','27472120','(623) 663-2338 x9984','Gottlieb, Harvey and Roberts','PLC',NULL,'pu27','ru31','hk43','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(37,'Katelynn Beahan','barrett11@hotmail.com',NULL,'$2y$10$rWYhtY7KN2N.E8.4.pqF9.yIDf03mYYJOEIrrpRnZV3TJ0RuQuW7a','58260404','863-384-7142','Mante, Auer and Walsh','PLC',NULL,'pq19','fo83','fs61','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(38,'Bessie Nitzsche DDS','ggislason@wolff.com',NULL,'$2y$10$a3imYLDGkA8r/3ew/hTA7uJMINPJuNImMesT.kgvXtak3ryByBC5S','78426763','1-834-765-6729 x85735','Douglas, Towne and Conn','LLC',NULL,'we99','ph38','yl89','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(39,'Ms. Crystel Zemlak PhD','nora69@rosenbaum.info',NULL,'$2y$10$c6dgkGQmvAGf2nO.hT5.Ju0vrFbsrUNNdgANze0LtoanXX0QVeG22','8361802','885.642.4014 x713','Heathcote, Feest and O\'Keefe','and Sons',NULL,'dq82','wz29','xs32','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(40,'Joan Bode','fschulist@hotmail.com',NULL,'$2y$10$6nFZq.Q2fa42C6Mbxf3.ZeMJakIxkI1SLCrCwV5QfyGb9mjFARPI2','84568005','820.882.0839','Sipes PLC','Ltd',NULL,'tq53','dc74','od13','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(41,'Weldon Shields','whill@nader.com',NULL,'$2y$10$0Y.ew9XTgoNDAeEOnQ3YcuZUXLqBL7C36upnwaPtFNaB8C03mj0Di','6275453','241-360-3093','Hirthe, Kemmer and Kris','PLC',NULL,'rl18','jv58','vu65','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(42,'Freddy Russel','choppe@graham.com',NULL,'$2y$10$O7U1cUM3fO7wV0gny69V4eG/bIJU97rtht3ZWBnvP56u6XPxqxROC','39274063','573-744-0778','Denesik Group','LLC',NULL,'lv51','qh83','ic58','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(43,'Jay Runolfsdottir','agislason@gmail.com',NULL,'$2y$10$UxJHT7Y1HgdbeCElY/PQYenoA2kx8xxjqKTI3Ui.i4HnUsyXi4iLy','18087510','795.821.4983','Bashirian, Cruickshank and McCullough','Inc',NULL,'is65','ch25','dl37','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(44,'Dr. Conner Olson IV','estevan.welch@gmail.com',NULL,'$2y$10$uXHiDC.pRgy61nQXMDBAuu9xJZJBp0bTXE75MM4HynBmUiNlaLcnK','35822181','910.724.0161','Kertzmann-Nolan','PLC',NULL,'yp16','ut49','pq83','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(45,'Mrs. Jakayla Dietrich V','don54@gmail.com',NULL,'$2y$10$28gZgSFi9uV699FLKPc7b.qM19wdbIIClV4B0t73VL/X3IjnYcsAu','89795398','1-839-626-9138 x0047','Hand LLC','Inc',NULL,'af74','ps17','rp97','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(46,'Marcelina Runolfsson','nicolas.martin@hintz.com',NULL,'$2y$10$d0W9FVv1YowjXlK87rLwu.J0Vm2muHzZDSEeYjS8TEdKANhSOsagG','32940685','920-861-1032 x9204','Goyette-Nolan','PLC',NULL,'vi28','yd83','jv68','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(47,'Dr. Solon Schoen II','kutch.robbie@gmail.com',NULL,'$2y$10$KnhyjsBUzyQxi4/M8c2Gs.h66Shx9VbEwVoV9GeZKgZVn2dZEaR.W','12072495','(956) 229-4284 x497','Veum Inc','PLC',NULL,'wi59','jb59','kb79','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(48,'Prof. Jerel Konopelski DDS','nikolaus.maci@yahoo.com',NULL,'$2y$10$jFNRwizMDmpZoUZrqcCrK.HprIN8CJm.1hWQyMr631KWlVK699T..','30215638','1-567-513-0502 x83572','Stokes-Lang','PLC',NULL,'io98','tv25','hp56','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(49,'Autumn Fahey','west.megane@gmail.com',NULL,'$2y$10$dvj4WMIgLNmt1OrMscnhquQuojnavB.5522YxEHGIGacbyudpbqs2','95097213','+1-430-586-0393','Blanda LLC','PLC',NULL,'xi44','fy34','ph71','reprobe','partner',NULL,'2020-01-24 22:51:10','2020-01-24 22:51:10',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(50,'Prof. Zoe O\'Keefe','hermann.cali@gmail.com',NULL,'$2y$10$PCs/T2stbIgW6agnqK/8oOUcZy4kXGADH2FIybnoeCMrqXbDx7Nxu','81511930','612.550.4372 x61178','Goodwin-Kozey','Ltd',NULL,'yu62','at37','vt28','reprobe','partner',NULL,'2020-01-24 22:51:11','2020-01-24 22:51:11',0,0,NULL,NULL,NULL,NULL,NULL,NULL),(51,'Mrs. Kaitlyn Abernathy I','marcelo56@gmail.com',NULL,'$2y$10$jWnF7UUAL6y8Z5rriSperuzHwv9TAhQghMG5Ed2tCbw1793BLtQyS','28682006','(812) 727-1069 x47890','Buckridge, Kreiger and Larson','PLC',NULL,'wg51','hl58','ov52','reprobe','partner',NULL,'2020-01-24 22:51:11','2020-01-24 22:51:11',0,0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-24 16:10:35
