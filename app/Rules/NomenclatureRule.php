<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NomenclatureRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // $parts = str_split($value,2);
        // $part1 = '';
        // $part2 = '';
        // for ($i=0; $i < count($value); $i++) { 
            
        // }
        // if(strlen ( $value ) == 4){
        //     if(is_numeric($parts[0])){
        //         $part1 = 'numeric';
        //     }else{
        //         $part1 = 'string';
        //     }
        //     if(is_numeric($parts[1])){
        //         $part2 = 'numeric';
        //     }else{
        //         $part2 = 'string';
        //     }        
    
        //     if($part1 == $part2){
        //         return false;
        //     }else{
        //         return true;
        //     }
        if(strlen ( $value ) == 4){
            $numeric = 0;
            $string = 0; 
            for ($i=0; $i < strlen($value); $i++) {
    
                if(is_numeric($value[$i])){
                    $numeric++;
                }else{
                    $string++;
                } 
            }
    
            if($numeric == 2 && $string == 2){
                return true;
            }else{
                return false;
            }
    
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El formato correcto para la nomenclatura es dos letras y dos numeros "AA01" o "01AA" o "1A1A" o "A1A1"' ;
    }
}
