<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nomenclature extends Model
{
    protected $fillable = [
        'code',
        'user_id',
        'selected'
    ];
}
