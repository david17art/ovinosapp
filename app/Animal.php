<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    protected $fillable = [
        'name',
        'birthdate',
        'gender',
        'weight',
        'parto',
        'dad_id',
        'mom_id',
        'tatto',
        'race_id',
        'status',
        'completed',
        'criador',
        'property',
        'generation',
        'otorgamiento',
        'clasificador',
        'payed',
        'user_id'
    ];

    public function partner(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function dad(){
        return $this->belongsTo(Animal::class,'dad_id');
    }

    public function mom(){
        return $this->belongsTo(Animal::class,'mom_id');
    }    

    public function raza(){
        return $this->belongsTo(Race::class,'race_id');
    }

    public function payment(){
        return $this->hasMany(Payment::class);
    }
}
