<?php

namespace App\Http\Controllers;

use App\Race;
use Illuminate\Http\Request;

class RaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.races.index');
    }

    public function get(){
        return Race::orderBy('id','desc')->get();
    }

    public function names(){
        $result = [];
        foreach(Race::all() as $animal){
            array_push($result,$animal->name);
        }
        return $result;
    }    

    public function verify(Request $request){
        $countRace = Race::where('name',$request->race)->count();
        if($countRace){
            return response()->json([
                "message" => $request->race." No Está disponible",
                "verify" => false
            ]);
        }else{
            return response()->json([
                "message" => $request->race." Está disponible",
                "verify" => true
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $race = Race::create($request->all());
        return response()->json([
            "message" => "Raza creada correctamente"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function show(Race $race)
    {
        return $race->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function edit(Race $race)
    {
        return $race->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $race = Race::findOrFail($id);
        $race->fill($request->all());
        $race->save();
        return response()->json([
            "message" => "Raza actualizada correctamente"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $race = Race::destroy($id);
        return response()->json([
            "message" => "Raza eliminada con exito"
        ]);
    }
}
