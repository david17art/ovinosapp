<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Payment;
use Illuminate\Support\Facades\Mail;
use App\Mail\AprobeMemberMail;
use App\Mail\ReprobeMemberMail;
use App\Mail\NotificationGeneralMail;
use PDF;
use Auth;

class UserController extends Controller
{
    public function index(){
        $users = User::where('role','<>','superuser')->where('status','aprobe')->get();
        return view('superusers.users.index',compact('users'));
    }
    public function me(){
        return Auth::user();
    }
    public function getUsers($role,$status){
        $users = User::orderBy('id','desc')->with('payment');
        if($role){
            $users->where('role',$role);
        }
        if($status && $role != 'admin' && $role != 'superuser'){
            $users->where('status',$status);
        }
        return $users->get();
    }

    public function aprobe(Request $request){
        $user = User::findOrFail($request->id);
        Mail::to($user->email)->send(new AprobeMemberMail($user));
        Mail::to($user->email)->send(new NotificationGeneralMail("Hemos recibido su pago de membresia de forma correcta",8));

        $payment = Payment::where('user_id',$user->id)->where('animal_id',null)->first();
        if($payment){
            $payment->status = 'aprobe';
            $payment->save();
        }


        $user->fill([
            "nomenclature" => $request->nomenclature,
            "status" => "aprobe",
            "payed" => true
        ]);
        $user->save();
        return response()->json([
            "message" => "Membresia aprobada correctamente"
        ]);
    }

    public function certificate($id){
        $user = User::findOrFail($id);
        $data = ['client' => $user];
        $pdf = PDF::loadView('pdfs.aprobeMember',$data);
        return $pdf->stream();
    }
    public function reprobe($id){
        $user = User::findOrFail($id);
        Mail::to($user->email)->send(new ReprobeMemberMail($user));
        User::destroy($id);
        return response()->json([
            "message" => "Membresia rechazada correctamente"
        ]);        
    }

    public function generateUsers(){
        factory(User::class, 48)->create();
        return redirect()->back();
    }

    public function store(Request $request){
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = 'admin';
        $user->save();
    }

    public function update(Request $request,$id){
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();
        return response()->json([
            "message" => "Usuario cambiado correctamente"
        ]);
    }
}
