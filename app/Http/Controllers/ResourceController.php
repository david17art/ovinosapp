<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Race;
use App\Animal;
use Auth;
use App\State;
use App\Town;
use App\Bank;

class ResourceController extends Controller
{
    public function auth(){
        return Auth::user();
    }
    public function races(){
        return Race::all();
    }

    public function fathers($gender){
        return Animal::where('status','aprobado')->where('gender',$gender)->get();
    }

    public function state(){
        return State::where('country_id',1)->get();
    }

    public function town($state){
        return Town::where('state_id',$state)->get();
    }

    public function banks(){
        return Bank::orderBy('name','asc')->get();
    }
    
}
