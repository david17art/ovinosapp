<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NomenclatureRequest;

class NomenclatureController extends Controller
{
    public function valid(NomenclatureRequest $request){
        return $request->nomenclature;
    }
}
