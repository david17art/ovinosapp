<?php

namespace App\Http\Controllers;

use App\Animal;
use Illuminate\Http\Request;
use Auth;
use PDF;
use App\Payment;

class AnimalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partner.animals.index');
    }

    public function get(){
        if(Auth::user()->role == 'partner'){
            return Animal::where('user_id',Auth::user()->id)->with('dad','mom','raza','payment')->get();
        }else{
            return Animal::with('partner','dad','mom','raza','payment')->get();
        }
    }

    public function getByStatus($status){
        if(Auth::user()->role == 'partner'){
            return Animal::where('user_id',Auth::user()->id)->where('id','>',4)->where('status',$status)->with('dad','mom','raza','payment')->get();
        }else{
            return Animal::with('partner','dad','mom','raza','payment')->where('id','>',4)->where('status',$status)->get();
        }
    }
    public function verify(Request $request){
        $countAnimal = Animal::where('name',$request->name)->count();
        if($countAnimal){
            return response()->json([
                "message" => $request->name." No Está disponible",
                "verify" => false
            ]);
        }else{
            return response()->json([
                "message" => $request->name." Está disponible",
                "verify" => true
            ]);
        }
    }

    public function names(){
        $result = [];
        foreach(Animal::all() as $animal){
            array_push($result,$animal->name);
        }
        return $result;
    }

    public function certificate($id){
        $animal = Animal::where('id',$id)->with('raza','partner','dad','dad.mom','dad.dad','mom','mom.dad','mom.mom')->first();
        // return $animal;
        $data = ['animal' => $animal];
        $pdf = PDF::loadView('pdfs.animalCertificate',$data);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream();        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function aprobeOrReprobe($id,$status){
        $animal = Animal::findOrFail($id);
        $animal->status = $status;
        

        // Aprobar o reprobar pago

        foreach ($animal['payment'] as $p) {
            $payment = Payment::findOrFail($p->id);
            if($status == 'aprobado'){
                $payment->status = "aprobe";
            }else if($status == 'rechazado'){
                $payment->status = "reprobe";
            }
            
            $payment->save();
        }
        $animal->save();

        return response()->json([
            'message' => "Cambio de estatus hecho correctamente"
        ]);
    }

    public function aproberOrReprobeAdicional(Request $request, $id,$status){
        $animal = Animal::findOrFail($id);
        $animal->status = $status;
        

        // Aprobar o reprobar pago

        foreach ($animal['payment'] as $p) {
            $payment = Payment::findOrFail($p->id);
            if($status == 'aprobado'){
                $payment->status = "aprobe";
            }else if($status == 'rechazado'){
                $payment->status = "reprobe";
            }
            
            $payment->save();
        }

        //Guardar los datos del tecnico clasificador

        $animal->clasificador = $request->clasificador;
        $animal->otorgamiento = date('Y-m-d');
        $animal->completed = true;
        $animal->payed = true;
        $animal->save();

        return response()->json([
            'message' => "Cambio de estatus hecho correctamente"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $animal = Auth::user()->animal()->create($request->all());
        $payment = Payment::create([
            "number" => $request['payment']['number'],
            "refer" => $request['payment']['refer'],
            "amount" => $request['payment']['amount'],
            "bank" => $request['payment']['bank'],
            "animal_id" => $animal->id
        ]);

        return response()->json([
            'message' => "Animal creado con exito"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Animal  $animal
     * @return \Illuminate\Http\Response
     */
    public function show(Animal $animal)
    {
        return $animal;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Animal  $animal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Animal::where('id',$id)->with('dad','mom','raza','partner','payment')->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Animal  $animal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $animal = Animal::findOrFail($id);
        $animal->fill($request->all());
        $animal->save();
        $payment = Payment::find($animal->payment->id);
        $payment->refer = $animal->payment->refer;
        $payment->amount = $animal->payment->amount;
        $payment->bank = $animal->payment->bank;
        $payment->save();
        return response()->json([
            "message" => "Animal actualizado correctamente"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Animal  $animal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Animal $animal)
    {
        $animal->delete();
        return response()->json([
            "message" => "Animal eliminado correctamente"
        ]);
    }
}
