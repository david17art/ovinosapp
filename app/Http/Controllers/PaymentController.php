<?php

namespace App\Http\Controllers;

use App\Payment;
use App\User;
use App\Animal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationGeneralMail;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('superusers.payments.index');
    }

    public function get($mode,$id){
        if($mode == 'member'){
            return Payment::where('user_id',$id)->get();
        }else if($mode == 'animal'){
            return Payment::where('animal_id',$id)->get();
        }else if($mode == 'animal_request'){
            return Payment::where('user_id',$id)->where('animal_module',true)->where('animal_id',null)->get();
        }
    }

    public function getByStatus($status,$type){
        // return Payment::where('status',$status)->with('partner','animal')->get();
        $payments = Payment::where('status',$status)->with('partner','animal','animal.partner')->get();
        $result = [];
        foreach ($payments as $payment) {
            if($payment->user_id && !$payment->animal_id && !$payment->animal_module){
                if($type == 'partner'){
                    array_push($result,$payment);
                }
            }else if($payment->animal_id && !$payment->user_id){
                if($type == 'animal'){
                    array_push($result,$payment);
                }
            }else if($payment->user_id && $payment->animal_module && !$payment->animal_id){
                if($type == 'animal_request'){
                    array_push($result,$payment);
                }
            }
        }
        return $result;
    }

    public function changeStatus($type,$id){
        $payment = Payment::where('id',$id)->first();
        $payment->status = $type;
        $payment->save();
        if($payment->user_id){
            $user = User::findOrFail($payment->user_id);
            if($type == 'aprobe'){
                
                $user->payed = true;
                if($payment->animal_module){
                    $user->animal_module = true;
                    Mail::to($user->email)->send(new NotificationGeneralMail("Hemos aprobado su pago",9));
                }else{
                    Mail::to($user->email)->send(new NotificationGeneralMail("Hemos aprobado su pago",4));
                }
            }else if($type == 'reprobe'){
                Mail::to($user->email)->send(new NotificationGeneralMail("Hemos rechazado su pago",3));
                if($payment->animal_module){
                    $user->animal_module = false;
                }
            }
            $user->save();
        }else if($payment->animal_id){
            $animal = Animal::findOrFail($payment->animal_id);
            if($type == 'aprobe'){
                $animal->payed = true;
            }else if($type == 'reprobe'){
                $animal->payed = false;
            }
            
            $animal->save();
        }
        return response()->json([
            "message" => "Estatus cambiado correctamente"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $payment = new Payment;
        $payment->number = $request->number;
        $payment->refer = $request->refer;
        $payment->amount = $request->amount;
        $payment->bank = $request->bank;
        if($request->mode == 'member'){
            $payment->user_id = $request->object_id;
        }else if($request->mode == 'animal'){
            $payment->animal_id = $request->object_id;
        }else if($request->mode == 'animal_request'){
            $payment->user_id = $request->object_id;
            $payment->animal_module = true;
        }
        Mail::to("david17art@gmail.com")->send(new NotificationGeneralMail("Se ha registrado un pago",2));
        $payment->save();
        return response()->json([
            "message" => "Pago registrado con exito"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        $payment->delete();
        return response()->json([
            "message" => "Pago eliminado correctamente"
        ]);
    }
}
