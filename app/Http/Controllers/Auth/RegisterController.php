<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Rules\NomenclatureRule;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationGeneralMail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'predio' => ['required','string'],
            'identification' => ['required','max:10'],
            'phone' => ['required'],
            'company' => ['required'],
            'nomenclatureSuggest1' => ['required','max:4',new NomenclatureRule],
            'nomenclatureSuggest2' => ['required','max:4',new NomenclatureRule],
            'nomenclatureSuggest3' => ['required','max:4',new NomenclatureRule]
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //Notificacion al asociado
        Mail::to($data['email'])->send(new NotificationGeneralMail("Su solicitud de membresia está siendo procesada",0));
        //Notificacion al email
        Mail::to(User::findOrFail(1)->email)->send(new NotificationGeneralMail("Le enviaron una solicitud de membresia",1));

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            "phone" => $data['phone'],
            "company" => $data['company'],
            "predio" => $data['predio'],
            "status" => "reprobe",
            "identification" => $data['identification'],
            "nomenclatureSuggest1" => $data['nomenclatureSuggest1'],
            "nomenclatureSuggest2" => $data['nomenclatureSuggest2'],
            "nomenclatureSuggest3" => $data['nomenclatureSuggest3'],
            "role" => 'partner'
        ]);
        
        $user->payment()->create([
            'refer' => $data['refer'],
            'amount' => $data['amount'],
            'bank' => $data['bank']
        ]);

        return $user;
    }
}
