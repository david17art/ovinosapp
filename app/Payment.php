<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'number',
        'refer',
        'amount',
        'bank',
        'user_id',
        'animal_id',
        'status',
        'animal_module'
    ];

    public function animal(){
        return $this->belongsTo(Animal::class,'animal_id');
    }

    public function partner(){
        return $this->belongsTo(User::class,'user_id');
    }

}
