<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
        'nomenclature',
        'identification',
        'nomenclatureSuggest1',
        'nomenclatureSuggest2',
        'nomenclatureSuggest3',
        'phone',
        'company',
        'predio',
        'status',
        'role',
        'completed',
        'state_id',
        'town_id',
        'city',
        'address',
        'zip',
        'payed',
        'animal_module'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function animal(){
        return $this->hasMany(Animal::class);
    }

    public function payment(){
        return $this->hasMany(Payment::class);
    }
}
