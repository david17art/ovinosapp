<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to('/home');
});

Route::get('/valid/{nomenclatura}',function($nomenclatura){
    if(strlen ( $nomenclatura ) == 4){
        $numeric = 0;
        $string = 0; 
        for ($i=0; $i < strlen($nomenclatura); $i++) {

            if(is_numeric($nomenclatura[$i])){
                $numeric++;
            }else{
                $string++;
            } 
        }

        if($numeric == 2 && $string == 2){
            return "Pasa";
        }else{
            return "No pasa";
        }

    }
});

Auth::routes();
Route::get('generate-users','UserController@generateUsers');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('test-email',function(){
    return view('mails.aprobeMember');
});

Route::prefix('members')->group(function () {
    Route::get('certificate/{id}','UserController@certificate');
});
Route::middleware(['auth'])->group(function(){
    // Recursos
    Route::prefix('resources')->group(function(){
        Route::get('races','ResourceController@races');
        Route::get('fathers/{gender}','ResourceController@fathers');
        Route::get('auth','ResourceController@auth');
        Route::get('states','ResourceController@state');
        Route::get('towns/{state}','ResourceController@town');
        Route::get('banks','ResourceController@banks');
    });
    //Pagos
    Route::prefix('payments')->group(function(){
        Route::get('get/{status}/type/{type}','PaymentController@getByStatus');
        Route::get('get/{mode}/{id}','PaymentController@get');
        Route::get('status/{type}/{id}','PaymentController@changeStatus');
    });
    Route::resource('payments','PaymentController');
    // Rutas para partners
    Route::prefix('partners')->group(function() {
        Route::prefix('animals')->group(function(){
            Route::get('get','AnimalController@get');
            Route::get('get/{status}','AnimalController@getByStatus');
            Route::get('certificate/{id}','AnimalController@certificate');
            Route::post('verify','AnimalController@verify');
            Route::get('names','AnimalController@names');
        });        
        Route::resource('animals','AnimalController');
        Route::prefix('users')->group(function(){
            Route::get('me','UserController@me');
            Route::put('/{id}','UserController@update');
        });
    });
    // Rutas que comparten el Super User y el Admin
    Route::prefix('superAdmin')->group(function () {
        Route::prefix('animals')->group(function(){
            Route::get('{id}/{mode}','AnimalController@aprobeOrReprobe');
            Route::post('{id}/{mode}','AnimalController@aproberOrReprobeAdicional');
        }); 
        Route::prefix('races')->group(function(){
            Route::get('get','RaceController@get');
            Route::post('verify','RaceController@verify');
            Route::get('names','RaceController@names');
        });
        Route::resource('races','RaceController');
        Route::prefix('users')->group(function () {
            Route::get('/','UserController@index');
            
            Route::get('changeRole','UserController@changeRole');
            Route::get('get/{role}/{status}','UserController@getUsers');
            Route::post('aprobe','UserController@aprobe');
            Route::get('reprobe/{id}','UserController@reprobe');
            Route::prefix('nomenclatures')->group(function(){
                Route::post('valid','NomenclatureController@valid');
            });
            Route::post('/','UserController@store');
            Route::put('/{id}','UserController@update');
        });
    });
});