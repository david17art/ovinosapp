<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompletedMemberOnTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('completed')->default(false);
            $table->boolean('payed')->default(false);
            $table->integer('country_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('town_id')->nullable();
            $table->string('city')->nullable();
            $table->text('address')->nullable();   
            $table->integer('zip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('completed');
            $table->dropColumn('country_id');
            $table->dropColumn('state_id');
            $table->dropColumn('town_id');
            $table->dropColumn('city');
            $table->dropColumn('address');
            
        });
    }
}
