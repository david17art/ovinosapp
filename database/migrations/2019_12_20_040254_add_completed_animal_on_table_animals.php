<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompletedAnimalOnTableAnimals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('animals', function (Blueprint $table) {
            $table->boolean('completed')->default(false);
            $table->string('criador')->nullable();
            $table->string('property')->nullable();
            $table->integer('generation')->nullable();
            $table->date('otorgamiento')->nullable();
            $table->string('clasificador')->nullable();
            $table->boolean('payed')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('animals', function (Blueprint $table) {
            $table->dropColumn('completed');
            $table->dropColumn('criador');
            $table->dropColumn('property');
            $table->dropColumn('generation');
            $table->dropColumn('ortorgamiento');
            $table->dropColumn('clasificador');
        });
    }
}
