<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('identification')->nullable();
            $table->string('phone')->nullable();
            $table->string('company')->nullable();
            $table->string('predio')->nullable();
            $table->string('nomenclature')->nullable();
            $table->string('nomenclatureSuggest1')->nullable();
            $table->string('nomenclatureSuggest2')->nullable();
            $table->string('nomenclatureSuggest3')->nullable();
            $table->enum('status',['aprobe','reprobe'])->nullable();
            $table->enum('role',['superuser','admin','partner'])->default('partner');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
