<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->enum('gender',['macho','hembra']);
            $table->date('birthdate');
            $table->float('weight');
            $table->enum('parto',['simple','double','triple','multiple']);
            $table->integer('dad_id')->nullable();
            $table->integer('mom_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('race_id');
            $table->string('tatto');
            $table->enum('status',['revision','aprobado','rechazado'])->default('revision');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
