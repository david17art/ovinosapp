<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    $n1 = chr(rand(ord('a'), ord('z'))).chr(rand(ord('a'), ord('z'))).rand(1, 9).rand(1, 9);
    $n2 = chr(rand(ord('a'), ord('z'))).chr(rand(ord('a'), ord('z'))).rand(1, 9).rand(1, 9);
    $n3 = chr(rand(ord('a'), ord('z'))).chr(rand(ord('a'), ord('z'))).rand(1, 9).rand(1, 9);
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt('password'),
        'phone' => $faker->phoneNumber,
        'predio' => $faker->companySuffix,
        'company' => $faker->company,
        'role' => 'partner',
        'status' => 'reprobe',
        'identification' => $faker->randomNumber(8, $strict = false),
        'nomenclatureSuggest1' => $n1,
        'nomenclatureSuggest2' => $n2,
        'nomenclatureSuggest3' => $n3,
    ];
});
