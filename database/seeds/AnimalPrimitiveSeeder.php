<?php

use Illuminate\Database\Seeder;
use App\Animal;

class AnimalPrimitiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Animal::create([
            'name' => 'Padre 1',
            'gender' => 'macho',
            'birthdate' => '2017-08-08',
            'weight' => '10',
            'parto' => 'simple',
            'race_id' => 1,
            'tatto' => "P1",
            'status' => 'aprobado'
        ]);
        Animal::create([
            'name' => 'Padre 2',
            'gender' => 'macho',
            'birthdate' => '2017-08-08',
            'weight' => '10',
            'parto' => 'simple',
            'race_id' => 2,
            'tatto' => "P1",
            'status' => 'aprobado'
        ]);        
        Animal::create([
            'name' => 'Madre 1',
            'gender' => 'hembra',
            'birthdate' => '2017-08-08',
            'weight' => '10',
            'parto' => 'simple',
            'race_id' => 2,
            'tatto' => "M1",
            'status' => 'aprobado'
        ]);              
        Animal::create([
            'name' => 'Madre 2',
            'gender' => 'hembra',
            'birthdate' => '2017-08-08',
            'weight' => '10',
            'parto' => 'simple',
            'race_id' => 2,
            'tatto' => "M2",
            'status' => 'aprobado'
        ]);                        
    }
}
