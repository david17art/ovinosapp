<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Administrador",
            "email" => "admin@ovinos.com",
            "password" => bcrypt('password'),
            "role" => "admin"
        ]);
        User::create([
            "name" => "Super User",
            "email" => "superuser@ovinos.com",
            "password" => bcrypt('password'),
            "role" => "superuser"
        ]);   
        User::create([
            "name" => "Asociado",
            "email" => "partner@ovinos.com",
            "password" => bcrypt('password'),
            "role" => "partner",
            'nomenclature' => "AA11",
            'status' => 'aprobe'
        ]);                
    }
}
