<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RaceSeeder::class);
        $this->call(AnimalPrimitiveSeeder::class);
        $this->call(UbicationSeeder::class);
        $this->call(BanksSeeder::class);
    }
}
