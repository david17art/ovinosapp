<?php

use Illuminate\Database\Seeder;
use App\Bank;

class BanksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Bank::create([
                    'code' => '0156',
                    'name' => '100%BANCO'
                ]);
                Bank::create([
                    'code' => '0196',
                    'name' => 'ABN AMRO BANK'
                ]);
                Bank::create([
                    'code' => '0172',
                    'name' => 'BANCAMIGA BANCO MICROFINANCIERO'
                ]);
                Bank::create([
                    'code' => '0171',
                    'name' => 'BANCO ACTIVO BANCO COMERCIAL'
                ]);
                Bank::create([
                    'code' => '0166',
                    'name' => 'BANCO AGRICOLA'
                ]);
                Bank::create([
                    'code' => '0175',
                    'name' => 'BANCO BICENTENARIO'
                ]);
                Bank::create([
                    'code' => '0128',
                    'name' => 'BANCO CARONI BANCO UNIVERSAL'
                ]);
                Bank::create([
                    'code' => '0102',
                    'name' => 'BANCO DE VENEZUELA'
                ]);
                Bank::create([
                    'code' => '0114',
                    'name' => 'BANCO DEL CARIBE'
                ]);
                Bank::create([
                    'code' => '0163',
                    'name' => 'BANCO DEL TESORO'
                ]);
                Bank::create([
                    'code' => '0115',
                    'name' => 'BANCO EXTERIOR'
                ]); 
                Bank::create([
                    'code' => '0173',
                    'name' => 'BANCO INTERNACIONAL DE DESARROLLO'
                ]);
                Bank::create([
                    'code' => '0105',
                    'name' => 'BANCO MERCANTIL'
                ]);
                Bank::create([
                    'code' => '0191',
                    'name' => 'BANCO NACIONAL DE CREDITO'
                ]);
                Bank::create([
                    'code' => '0116',
                    'name' => 'BANCO OCCIDENTAL DE DESCUENTO'
                ]);
                Bank::create([
                    'code' => '0138',
                    'name' => 'BANCO PLAZA'
                ]);
                Bank::create([
                    'code' => '0108',
                    'name' => 'BANCO PROVINCIAL BBVA'
                ]);
                Bank::create([
                    'code' => '0104',
                    'name' => 'BANCO VENEZOLANO DE CREDITO'
                ]);
                Bank::create([
                    'code' => '0168',
                    'name' => 'BANCRECER BANCO DE DESARROLLO'
                ]);
                Bank::create([
                    'code' => '0134',
                    'name' => 'BANESCO BANCO UNIVERSAL'
                ]);
                Bank::create([
                    'code' => '0146',
                    'name' => 'BANGENTE'
                ]);
                Bank::create([
                    'code' => '0174',
                    'name' => 'BANPLUS BANCO COMERCIAL'
                ]);
                Bank::create([
                    'code' => '0190',
                    'name' => 'CITIBANK'
                ]);
                Bank::create([
                    'code' => '0157',
                    'name' => 'DELSUR BANCO UNIVERSAL'
                ]);
                Bank::create([
                    'code' => '0151',
                    'name' => 'BANCO FONDO COMUN'
                ]);
                Bank::create([
                    'code' => '0137',
                    'name' => 'SOFITASA'
                ]);

         
    }
}
