<?php

use Illuminate\Database\Seeder;
use App\Race;

class RaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Race::create([
            'name' => 'Raza 1'
        ]);
        Race::create([
            'name' => 'Raza 2'
        ]);        
        Race::create([
            'name' => 'Raza 3'
        ]);        
    }
}
